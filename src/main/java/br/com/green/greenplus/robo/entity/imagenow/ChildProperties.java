package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class ChildProperties {

    private String id;
    private String name;
    private String type;
    private String value;

    public String getId() {return (id == null ? "" : id);}
    public void setId(String id) {this.id = id;}

    public String getName() {return (name == null ? "" : name);}
    public void setName(String name) {this.name = name;}

    public String getType() {return (type == null ? "" : type);}
    public void setType(String type) {this.type = type;}

    public String getValue() {return (value == null ? "" : value);}
    public void setValue(String value) {this.value = value;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("id: %s ", id));
        sb.append(String.format("name: %s ", name));
        sb.append(String.format("type: %s ", type));
        sb.append(String.format("value: %s ", value));
        return sb.toString();
    }
}
