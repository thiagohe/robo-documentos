package br.com.green.greenplus.robo.tipos.load;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.entity.OTDDocCp;
import br.com.green.greenplus.robo.entity.OTDDocument;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.entity.Document;
import br.com.green.greenplus.robo.entity.OTDAtendimento;
import br.com.green.greenplus.robo.entity.imagenow.Properties;
import br.com.green.greenplus.robo.main.Manager;

public class ProcessJobCMP implements Callable<Boolean> {

	private final LoadTO loadTO;
	private final List<OTDImagenow> documentos;

	public ProcessJobCMP(LoadTO loadTO, List<OTDImagenow> documentos) {
		this.loadTO = loadTO;
		this.documentos = documentos;
	}

	@Override
	public Boolean call() throws Exception {
		String filaDestino = null;
		for (OTDImagenow documento : documentos) {
			boolean sucessoImagenow = false;
			Document documentoImagenow = null;
			try {
				// ***************************************************************************
				// Consulta o documento no Imagenow
				// ***************************************************************************
				documentoImagenow = loadTO.getImagenowUtils().listarDocumento(documento.getDocId(), loadTO.getSessionHash());
				sucessoImagenow = true;
				Properties propriedadeSubtipo = loadTO.getImagenowUtils().getCustomProperty(documentoImagenow, documento.getDocTypeName());

				// ***************************************************************************
				// Remove o documento e suas propriedades do banco de dados
				// (caso existam)
				// ***************************************************************************
				loadTO.getDatabaseUtils().removerGpDocument(documento.getDocId());

				Map<String, String> dados = loadTO.getDatabaseUtils().listarDadosCliente(documento.getCreator());
				String idGrupo = dados.get("grupo");
				String idCliente = dados.get("id_cliente");
				String idCompartimento = dados.get("compartimento");
				if (idGrupo == null || idCliente == null) {
					throw new Exception("Cliente/grupo não encontrados no banco de dados da GREEN");
				}

				// ***************************************************************************
				// Sempre consulta pelo paciente
				// ***************************************************************************
				OTDAtendimento otdAtendimento;
				String cdPaciente = documento.getF1();
				otdAtendimento = loadTO.getWebserviceUtils().obterAtendimentoCMP(cdPaciente, Long.parseLong(loadTO.getCliente().getId()), loadTO.getDatabaseUtils());

				if (otdAtendimento == null) {
					throw new Exception();
				}

				documento.setF3(validateAndFormatDate(documento.getF3()));

				OTDDocument d = new OTDDocument();
				d.setIdGroup(idGrupo);
				d.setDocId(documento.getDocId());
				d.setFolder(documento.getF1());
				d.setTab(documento.getF2());
				d.setF3(documento.getF3());
				d.setF4(documento.getF4());
				d.setF5(documento.getF5());
				d.setDocTypeId(documento.getDocTypeId());
				d.setDocTypeName(documento.getDocTypeName());
				d.setCreator(documento.getCreator());
				d.setSubTypeName(propriedadeSubtipo.getValue());

				// ***************************************************************************
				// Muda a data de criação do documento para o formato correto
				// ***************************************************************************
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date creationDate = sdf.parse(documento.getCreationDate());
				sdf.applyPattern("d-M-yyyy");
				d.setCreationDate(sdf.format(creationDate));

				// ***************************************************************************
				// Inicializa a variável que vai conter os inserts da aplicação
				// ***************************************************************************
				StringBuilder sql = new StringBuilder();
				sql.append("DECLARE v_document_id gp_document.id%TYPE := seq_gp_document.NEXTVAL; ");
				sql.append("BEGIN ");

				// ***************************************************************************
				// Monta o insert em GP_DOCUMENT
				// ***************************************************************************
				sql.append(loadTO.getDatabaseUtils().inserirGpDocument(d));

				// ***************************************************************************
				// Consulta as custom properties que o cliente utiliza
				// ***************************************************************************
				Map<String, Integer> propriedades = loadTO.getDatabaseUtils().listarCustomProperties(idCompartimento, idCliente);
				if (campoValido(propriedades, "CP_DATA_ATENDIMENTO", documento.getF3())) {
					Integer id = propriedades.get("CP_DATA_ATENDIMENTO");
					String valor = documento.getF3();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_DATA_NASCIMENTO", otdAtendimento.getDtNascimento())) {
					Integer id = propriedades.get("CP_DATA_NASCIMENTO");
					String valor = validateAndFormatDate(otdAtendimento.getDtNascimento().replace("-", "/"));
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_IDENTIDADE", otdAtendimento.getNrIdentidade())) {
					Integer id = propriedades.get("CP_IDENTIDADE");
					String valor = otdAtendimento.getNrIdentidade();
					sql.append(montarInsertGpDocCp(id, valor));

					// ***************************************************************************
					// Atuaiza chave do documento
					// ***************************************************************************
					documento.setF2(valor);

					sql.append(loadTO.getDatabaseUtils().atualizarF2GpDocument(valor));
				}
				if (campoValido(propriedades, "CP_NOME_MAE", otdAtendimento.getNmMae())) {
					Integer id = propriedades.get("CP_NOME_MAE");
					String valor = otdAtendimento.getNmMae();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_NOME_PACIENTE", otdAtendimento.getNmPaciente())) {
					Integer id = propriedades.get("CP_NOME_PACIENTE");
					String valor = otdAtendimento.getNmPaciente();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_CD_PACIENTE", otdAtendimento.getCdPaciente())) {
					Integer id = propriedades.get("CP_CD_PACIENTE");
					String valor = documento.getF1();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_TIPO_DOCUMENTO", documento.getDocTypeName())) {
					Integer id = propriedades.get("CP_TIPO_DOCUMENTO");
					String valor = documento.getDocTypeName();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_NOME_CRIADOR", documento.getCreator())) {
					Integer id = propriedades.get("CP_NOME_CRIADOR");
					String valor = documento.getCreator();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				sql.append(" END; ");
				loadTO.getDatabaseUtils().executar(sql);
				documento.setNotes("");
				filaDestino = loadTO.getCliente().getFilaSucesso();
				Manager.logger.info(String.format("Documento inserido com sucesso: [%s]", documento.getDocId()));
			} catch (Exception e) {
				filaDestino = loadTO.getCliente().getFilaErro();
				Manager.logger.error(e.getMessage(), e);
				documento.setNotes((e.getMessage()).replace("\n", " "));
				Manager.logger.info(String.format("Documento com erro: [%s]", documento.getDocId()));
			} finally {
				if (sucessoImagenow) {
					try {
						loadTO.getImagenowUtils().atualizarChavesImagenow(documento, documentoImagenow, loadTO.getSessionHash());
					} catch (Exception e) {
						Manager.logger.error(e.getMessage(), e);
					}
				}
				loadTO.getImagenowUtils().rotearDocumentoImagenow(documento.getWorkflowItemId(), filaDestino, loadTO);
			}
		}

		return true;
	}

	private String validateAndFormatDate(String dateString) throws Exception {
		LocalDate localDate = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("d/M/yyyy"));
		return localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	private Boolean campoValido(Map<String, Integer> propriedades, String chave, Object valor) {
		if (propriedades.isEmpty() || valor == null) {
			return false;
		}

		Boolean existeNoMapa = propriedades.containsKey(chave);
		return (existeNoMapa);
	}

	private String montarInsertGpDocCp(Integer id, String valor) throws Exception {
		OTDDocCp cp = new OTDDocCp();
		cp.setCpId(id);
		cp.setValue(valor);
		return (loadTO.getDatabaseUtils().inserirGpDocCp(cp));
	}
}