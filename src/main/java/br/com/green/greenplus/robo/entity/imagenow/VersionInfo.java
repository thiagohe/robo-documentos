package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class VersionInfo {

    private String versionNumber;
    private String versionId;
    private Boolean isProvisional;

    public String getVersionNumber() {return (versionNumber == null ? "" : versionNumber);}
    public void setVersionNumber(String versionNumber) {this.versionNumber = versionNumber;}

    public String getVersionId() {return (versionId == null ? "" : versionId);}
    public void setVersionId(String versionId) {this.versionId = versionId;}

    public Boolean getIsProvisional() {return (isProvisional == null ? false : isProvisional);}
    public void setIsProvisional(Boolean isProvisional) {this.isProvisional = isProvisional;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("versionNumber: %s ", versionNumber));
        sb.append(String.format("versionId: %s ", versionId));
        sb.append(String.format("isProvisional: %s ", isProvisional));
        return sb.toString();
    }
}
