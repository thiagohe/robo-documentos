package br.com.green.greenplus.robo.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class OTDAtendimentoLeforte implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3411378773115086038L;
	@SerializedName("CD_PACIENTE")
    private String cdPaciente;
    @SerializedName("TP_ATENDIMENTO")
    private String tpAtendimento;
    @SerializedName("CD_CONVENIO")
    private String cdConvenio;
    @SerializedName("CD_CON_PLA")
    private String cdPlano;
    @SerializedName("CD_SUB_PLANO")
    private String cdSubPlano;
    @SerializedName("CD_CID")
    private String cdCid;
    @SerializedName("CD_PRO_INT")
    private String cdProcedimento;
    @SerializedName("NR_GUIA")
    private String nrGuia;
    @SerializedName("SENHA_SUS")
    private String senhaSus;
    @SerializedName("HR_ATENDIMENTO")
    private String dtAtendimento;
    @SerializedName("NM_PACIENTE")
    private String nmPaciente;
    @SerializedName("NR_IDENTIDADE")
    private String nrIdentidade;
    @SerializedName("NM_MAE")
    private String nmMae;
    @SerializedName("DT_NASCIMENTO")
    private String dtNascimento;
    @SerializedName("NR_CARTEIRA")
    private String nrCarteira;
    @SerializedName("NM_PRESTADOR")
    private String nmPrestador;
    @SerializedName("NM_CONVENIO")
    private String nmConvenio;
    @SerializedName("DS_CON_PLA")
    private String dsPlano;
    @SerializedName("NM_RESPONSAVEL")
    private String nmResponsavel;
    @SerializedName("CD_MULTI_EMPRESA")
    private String cdMultiEmpresa;

    public String getCdPaciente() {return (cdPaciente == null ? "" : cdPaciente);}
    public void setCdPaciente(String cdPaciente) {this.cdPaciente = cdPaciente;}

    public String getTpAtendimento() {return (tpAtendimento == null ? "" : tpAtendimento);}
    public void setTpAtendimento(String tpAtendimento) {this.tpAtendimento = tpAtendimento;}

    public String getCdConvenio() {return (cdConvenio == null ? "" : cdConvenio);}
    public void setCdConvenio(String cdConvenio) {this.cdConvenio = cdConvenio;}

    public String getCdPlano() {return (cdPlano == null ? "" : cdPlano);}
    public void setCdPlano(String cdPlano) {this.cdPlano = cdPlano;}

    public String getCdSubPlano() {return (cdSubPlano == null ? "" : cdSubPlano);}
    public void setCdSubPlano(String cdSubPlano) {this.cdSubPlano = cdSubPlano;}

    public String getCdCid() {return (cdCid == null ? "" : cdCid);}
    public void setCdCid(String cdCid) {this.cdCid = cdCid;}

    public String getCdProcedimento() {return (cdProcedimento == null ? "" : cdProcedimento);}
    public void setCdProcedimento(String cdProcedimento) {this.cdProcedimento = cdProcedimento;}

    public String getNrGuia() {return (nrGuia ==  null ? "" : nrGuia);}
    public void setNrGuia(String nrGuia) {this.nrGuia = nrGuia;}

    public String getSenhaSus() {return (senhaSus == null ? "" : senhaSus);}
    public void setSenhaSus(String senhaSus) {this.senhaSus = senhaSus;}

    public String getDtAtendimento() {return (dtAtendimento == null ? "" : dtAtendimento);}
    public void setDtAtendimento(String dtAtendimento) {this.dtAtendimento = dtAtendimento;}

    public String getNmPaciente() {return (nmPaciente == null ? "" : nmPaciente);}
    public void setNmPaciente(String nmPaciente) {this.nmPaciente = nmPaciente;}

    public String getNrIdentidade() {return (nrIdentidade == null ? "" : nrIdentidade);}
    public void setNrIdentidade(String nrIdentidade) {this.nrIdentidade = nrIdentidade;}

    public String getNmMae() {return (nmMae == null ? "" : nmMae);}
    public void setNmMae(String nmMae) {this.nmMae = nmMae;}

    public String getDtNascimento() {return (dtNascimento == null ? "" : dtNascimento);}
    public void setDtNascimento(String dtNascimento) {this.dtNascimento = dtNascimento;}

    public String getNrCarteira() {return (nrCarteira == null ? "" : nrCarteira);}
    public void setNrCarteira(String nrCarteira) {this.nrCarteira = nrCarteira;}

    public String getNmPrestador() {return (nmPrestador == null ? "" : nmPrestador);}
    public void setNmPrestador(String nmPrestador) {this.nmPrestador = nmPrestador;}

    public String getNmConvenio() {return (nmConvenio == null ? "" : nmConvenio);}
    public void setNmConvenio(String nmConvenio) {this.nmConvenio = nmConvenio;}

    public String getDsPlano() {return (dsPlano == null ? "" : dsPlano);}
    public void setDsPlano(String dsPlano) {this.dsPlano = dsPlano;}

    public String getNmResponsavel() {return (nmResponsavel == null ? "" : nmResponsavel);}
    public void setNmResponsavel(String nmResponsavel) {this.nmResponsavel = nmResponsavel;}

    public String getCdMultiEmpresa() {return (cdMultiEmpresa == null ? "" : cdMultiEmpresa);}
    public void setCdMultiEmpresa(String cdMultiEmpresa) {this.cdMultiEmpresa = cdMultiEmpresa;}
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("cdPaciente: %s ", cdPaciente));
        sb.append(String.format("tpAtendimento: %s ", tpAtendimento));
        sb.append(String.format("cdConvenio: %s ", cdConvenio));
        sb.append(String.format("cdPlano: %s ", cdPlano));
        sb.append(String.format("cdSubPlano: %s ", cdSubPlano));
        sb.append(String.format("cdCid: %s ", cdCid));
        sb.append(String.format("cdProcedimento: %s ", cdProcedimento));
        sb.append(String.format("nrGuia: %s ", nrGuia));
        sb.append(String.format("senhaSus: %s ", senhaSus));
        sb.append(String.format("dtAtendimento: %s ", dtAtendimento));
        sb.append(String.format("nmPaciente: %s ", nmPaciente));
        sb.append(String.format("nrIdentidade: %s ", nrIdentidade));
        sb.append(String.format("nmMae: %s ", nmMae));
        sb.append(String.format("nrCarteira: %s ", nrCarteira));
        sb.append(String.format("nmPrestador: %s ", nmPrestador));
        sb.append(String.format("nmConvenio: %s ", nmConvenio));
        sb.append(String.format("dsPlano: %s ", dsPlano));
        sb.append(String.format("nmResponsavel: %s ", nmResponsavel));
        sb.append(String.format("cdMultiEmpresa: %s ", cdMultiEmpresa));
        return sb.toString();
    }
}
