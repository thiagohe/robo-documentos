package br.com.green.greenplus.robo.entity.imagenow;

import java.util.List;

/**
 *
 * @author renato.paiva
 */
public class Properties {

    private String id;
    private String name;
    private String elementId;
    private String type;
    private String value;
    private List<ChildProperties> childProperties;

    public String getId() {return (id == null ? "" : id);}
    public void setId(String id) {this.id = id;}

    public String getName() {return (name == null ? "" : name);}
    public void setName(String name) {this.name = name;}

    public String getElementId() {return (elementId == null ? "" : elementId);}
    public void setElementId(String elementId) {this.elementId = elementId;}

    public String getType() {return (type == null ? "" : type);}
    public void setType(String type) {this.type = type;}

    public String getValue() {return (value == null ? "" : value);}
    public void setValue(String value) {this.value = value;}

    public List<ChildProperties> getChildProperties() {return childProperties;}
    public void setChildProperties(List<ChildProperties> childProperties) {this.childProperties = childProperties;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("id: %s ", id));
        sb.append(String.format("name: %s ", name));
        sb.append(String.format("elementId: %s ", elementId));
        sb.append(String.format("type: %s ", type));
        sb.append(String.format("value: %s ", value));
        return sb.toString();
    }
}
