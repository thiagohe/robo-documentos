package br.com.green.greenplus.robo.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author renato.paiva
 */
public class Settings {

    private static Properties properties;

    /**
     * Retorna uma propriedade contida no arquivo settings.properties.
     *
     * @param key (Nome da propriedade)
     * @return String (Valor da propriedade)
     */
    public static String getProperty(String key) {
        if (properties == null) {
            properties = new Properties();
            InputStream in = Settings.class.getResourceAsStream("/settings.properties");
            try {
                properties.load(in);
            } catch (IOException e) {
                Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return properties.getProperty(key);
    }
}
