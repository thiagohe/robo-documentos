package br.com.green.greenplus.robo.utils;

import java.util.List;

import br.com.green.greenplus.robo.entity.Document;
import br.com.green.greenplus.robo.entity.INKeys;
import br.com.green.greenplus.robo.entity.INProperty;
import br.com.green.greenplus.robo.entity.imagenow.Properties;
import br.com.green.greenplus.robo.entity.imagenow.RouteInfo;

/**
 *
 * @author renato.paiva
 */
public class JsonUtils {

    /**
     * Cria uma string em formato JSON para inserir um documento no Imagenow via
     * integration server.
     *
     * @param documento (Documento do Imagenow)
     * @return String (JSON para inserir o documento)
     * @throws Exception
     */
    public String documentToJson(Document documento) throws Exception {
        StringBuilder propertiesJson = new StringBuilder("");
        String separador = "";
        for (Properties p : documento.getProperties()) {
            propertiesJson.append(separador);
            propertiesJson.append("{");
            propertiesJson.append(String.format("\"id\": \"%s\",", p.getId()));
            propertiesJson.append(String.format("\"name\": \"%s\",", p.getName()));
            propertiesJson.append(String.format("\"type\": \"%s\",", p.getType()));
            propertiesJson.append(String.format("\"value\": \"%s\",", p.getValue()));
            propertiesJson.append("\"childProperties\": null");
            propertiesJson.append("}");
            separador = ",";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"info\": {");
        sb.append(String.format("\"name\": \"%s\",", documento.getInfo().getName()));
        sb.append(String.format("\"locationId\": \"%s\",", documento.getInfo().getKeys().getDrawerId()));
        sb.append(String.format("\"notes\": \"%s\",", documento.getInfo().getNotes()));
        sb.append("\"keys\": {");
        sb.append(String.format("\"drawer\": \"%s\",", documento.getInfo().getKeys().getDrawer()));
        sb.append(String.format("\"field1\": \"%s\",", documento.getInfo().getKeys().getField1()));
        sb.append(String.format("\"field2\": \"%s\",", documento.getInfo().getKeys().getField2()));
        sb.append(String.format("\"field3\": \"%s\",", documento.getInfo().getKeys().getField3()));
        sb.append(String.format("\"field4\": \"%s\",", documento.getInfo().getKeys().getField4()));
        sb.append(String.format("\"field5\": \"%s\",", documento.getInfo().getKeys().getField5()));
        sb.append(String.format("\"documentType\": \"%s\"", documento.getInfo().getKeys().getDocumentType()));
        sb.append("}");
        sb.append("},");
        sb.append("\"properties\": [");
        sb.append(propertiesJson);
        sb.append("]");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Cria uma string em formato JSON para mover um documento para uma fila no
     * Imagenow via integration server.
     *
     * @param wf (Dados do workflow)
     * @return String (JSON para mover o documento para a fila especificada)
     * @throws Exception
     */
    public String workflowToJson(RouteInfo wf) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(String.format("\"originWorkflowQueueId\": \"%s\",", wf.getOriginWorkflowQueueId()));
        sb.append(String.format("\"originWorkflowQueueName\": \"%s\",", wf.getOriginWorkflowQueueName()));
        sb.append(String.format("\"destinationWorkflowQueueId\": \"%s\",", wf.getDestinationWorkflowQueueId()));
        sb.append(String.format("\"routeType\": \"%s\",", wf.getRouteType()));
        sb.append(String.format("\"reason\": \"%s\"", wf.getReason()));
        sb.append("}");
        return sb.toString();
    }
    
    /**
     * Cria uma string em formato JSON para inserir um documento no Imagenow via
     * integration server.
     *
     * @param chaves (Chaves do documento que será criado)
     * @param customProperties (Lista de custom properties do documento que será
     * criado)
     * @return String (JSON para inserir o documento)
     * @throws CriarJsonDocumentoException
     */
    public String criarJsonDocumento(INKeys chaves, List<INProperty> customProperties) throws Exception {
        String retorno = null;

        StringBuilder propriedades = new StringBuilder("");
        String separador = "";

        //***************************************************************************
        // Monta as custom properties do documento
        //***************************************************************************
        if (customProperties!=null) {
        	for (INProperty inProperty : customProperties) {
                propriedades.append(separador);
                propriedades.append("{");
                propriedades.append(String.format("\"id\": \"%s\",", inProperty.getId()));
                propriedades.append(String.format("\"type\": \"%s\",", inProperty.getType()));
                propriedades.append(String.format("\"value\": \"%s\",", inProperty.getValue()));
                propriedades.append("\"childProperties\": null");
                propriedades.append("}");
                separador = ",";
            }
        }

        //***************************************************************************
        // Monta a estrutura do documento
        //***************************************************************************
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"info\": {");
        sb.append(String.format("\"name\": \"%s\",", chaves.getName()));
        sb.append(String.format("\"locationId\": \"%s\",", chaves.getDrawerId()));
        sb.append("\"keys\": {");
        sb.append(String.format("\"drawer\": \"%s\",", chaves.getDrawer()));
        sb.append(String.format("\"field1\": \"%s\",", chaves.getField1()));
        sb.append(String.format("\"field2\": \"%s\",", chaves.getField2()));
        sb.append(String.format("\"field3\": \"%s\",", chaves.getField3()));
        sb.append(String.format("\"field4\": \"%s\",", chaves.getField4()));
        sb.append(String.format("\"field5\": \"%s\",", chaves.getField5()));
        sb.append(String.format("\"documentType\": \"%s\"", chaves.getDocumentType()));
        sb.append("}");
        sb.append("},");
        sb.append("\"properties\": [");
        sb.append(propriedades);
        sb.append("]");
        sb.append("}");
        retorno = sb.toString();

        if (retorno == null) {
            throw new Exception("Erro ao montar json do documento.");
        }

        return retorno;
    }
}
