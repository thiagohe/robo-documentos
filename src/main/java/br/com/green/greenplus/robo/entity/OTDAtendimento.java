package br.com.green.greenplus.robo.entity;

/**
 *
 * @author renato.paiva
 */
public class OTDAtendimento {

	private String cdAtendimento;
	private String cdPaciente;
	private String tpAtendimento;
	private String cdConvenio;
	private String cdConPlan;
	private String cdSubPlano;
	private String cdCid;
	private String cdProcedimento;
	private String nrGuia;
	private String senhaSus;
	private String hrAtendimento;
	private String nrCpf;
	private String nmPaciente;
	private String nrIdentidade;
	private String nmMae;
	private String dtNascimento;
	private String nrCarteira;
	private String nmPrestador;
	private String nmConvenio;
	private String dsConPla;
	private String nmResponsavel;
	private String nrCns;

	public String getCdAtendimento() {
		return cdAtendimento;
	}

	public void setCdAtendimento(String cdAtendimento) {
		this.cdAtendimento = cdAtendimento;
	}

	public String getCdPaciente() {
		return cdPaciente;
	}

	public void setCdPaciente(String cdPaciente) {
		this.cdPaciente = cdPaciente;
	}

	public String getTpAtendimento() {
		return tpAtendimento;
	}

	public void setTpAtendimento(String tpAtendimento) {
		this.tpAtendimento = tpAtendimento;
	}

	public String getCdConvenio() {
		return cdConvenio;
	}

	public void setCdConvenio(String cdConvenio) {
		this.cdConvenio = cdConvenio;
	}

	public String getCdConPlan() {
		return cdConPlan;
	}

	public void setCdConPlan(String cdConPlan) {
		this.cdConPlan = cdConPlan;
	}

	public String getCdSubPlano() {
		return cdSubPlano;
	}

	public void setCdSubPlano(String cdSubPlano) {
		this.cdSubPlano = cdSubPlano;
	}

	public String getCdCid() {
		return cdCid;
	}

	public void setCdCid(String cdCid) {
		this.cdCid = cdCid;
	}

	public String getCdProcedimento() {
		return cdProcedimento;
	}

	public void setCdProcedimento(String cdProcedimento) {
		this.cdProcedimento = cdProcedimento;
	}

	public String getNrGuia() {
		return nrGuia;
	}

	public void setNrGuia(String nrGuia) {
		this.nrGuia = nrGuia;
	}

	public String getSenhaSus() {
		return senhaSus;
	}

	public void setSenhaSus(String senhaSus) {
		this.senhaSus = senhaSus;
	}

	public String getHrAtendimento() {
		return hrAtendimento;
	}

	public void setHrAtendimento(String hrAtendimento) {
		this.hrAtendimento = hrAtendimento;
	}

	public String getNrCpf() {
		return nrCpf;
	}

	public void setNrCpf(String nrCpf) {
		this.nrCpf = nrCpf;
	}

	public String getNmPaciente() {
		return nmPaciente;
	}

	public void setNmPaciente(String nmPaciente) {
		this.nmPaciente = nmPaciente;
	}

	public String getNrIdentidade() {
		return nrIdentidade;
	}

	public void setNrIdentidade(String nrIdentidade) {
		this.nrIdentidade = nrIdentidade;
	}

	public String getNmMae() {
		return nmMae;
	}

	public void setNmMae(String nmMae) {
		this.nmMae = nmMae;
	}

	public String getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getNrCarteira() {
		return nrCarteira;
	}

	public void setNrCarteira(String nrCarteira) {
		this.nrCarteira = nrCarteira;
	}

	public String getNmPrestador() {
		return nmPrestador;
	}

	public void setNmPrestador(String nmPrestador) {
		this.nmPrestador = nmPrestador;
	}

	public String getNmConvenio() {
		return nmConvenio;
	}

	public void setNmConvenio(String nmConvenio) {
		this.nmConvenio = nmConvenio;
	}

	public String getDsConPla() {
		return dsConPla;
	}

	public void setDsConPla(String dsConPla) {
		this.dsConPla = dsConPla;
	}

	public String getNmResponsavel() {
		return nmResponsavel;
	}

	public void setNmResponsavel(String nmResponsavel) {
		this.nmResponsavel = nmResponsavel;
	}

	public String getNrCns() {
		return nrCns;
	}

	public void setNrCns(String nrCns) {
		this.nrCns = nrCns;
	}

	@Override
	public String toString() {
		return "OTDAtendimento [cdAtendimento=" + cdAtendimento + ", cdPaciente=" + cdPaciente + ", tpAtendimento="
				+ tpAtendimento + ", cdConvenio=" + cdConvenio + ", cdConPlan=" + cdConPlan + ", cdSubPlano="
				+ cdSubPlano + ", cdCid=" + cdCid + ", cdProcedimento=" + cdProcedimento + ", nrGuia=" + nrGuia
				+ ", senhaSus=" + senhaSus + ", hrAtendimento=" + hrAtendimento + ", nrCpf=" + nrCpf + ", nmPaciente="
				+ nmPaciente + ", nrIdentidade=" + nrIdentidade + ", nmMae=" + nmMae + ", dtNascimento=" + dtNascimento
				+ ", nrCarteira=" + nrCarteira + ", nmPrestador=" + nmPrestador + ", nmConvenio=" + nmConvenio
				+ ", dsConPla=" + dsConPla + ", nmResponsavel=" + nmResponsavel + "]";
	}
}