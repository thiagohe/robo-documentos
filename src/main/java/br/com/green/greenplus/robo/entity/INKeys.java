package br.com.green.greenplus.robo.entity;

public class INKeys {

    private String name;
    private String drawerId;
    private String drawer;
    private String field1;
    private String field2;
    private String field3;
    private String field4;
    private String field5;
    private String documentType;

    public String getName() {return (name == null ? "" : name);}
    public void setName(String name) {this.name = name;}

    public String getDrawerId() {return (drawerId == null ? "" : drawerId);}
    public void setDrawerId(String drawerId) {this.drawerId = drawerId;}

    public String getDrawer() {return (drawer == null ? "" : drawer);}
    public void setDrawer(String drawer) {this.drawer = drawer;}

    public String getField1() {return (field1 == null ? "" : field1);}
    public void setField1(String field1) {this.field1 = field1;}

    public String getField2() {return (field2 == null ? "" : field2);}
    public void setField2(String field2) {this.field2 = field2;}

    public String getField3() {return (field3 == null ? "" : field3);}
    public void setField3(String field3) {this.field3 = field3;}

    public String getField4() {return (field4 == null ? "" : field4);}
    public void setField4(String field4) {this.field4 = field4;}

    public String getField5() {return (field5 == null ? "" : field5);}
    public void setField5(String field5) {this.field5 = field5;}

    public String getDocumentType() {return (documentType == null ? "" : documentType);}
    public void setDocumentType(String documentType) {this.documentType = documentType;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("name: %s ", name));
        sb.append(String.format("drawerId: %s ", drawerId));
        sb.append(String.format("drawer: %s ", drawer));
        sb.append(String.format("field1: %s ", field1));
        sb.append(String.format("field2: %s ", field2));
        sb.append(String.format("field3: %s ", field3));
        sb.append(String.format("field4: %s ", field4));
        sb.append(String.format("field5: %s ", field5));
        sb.append(String.format("documentType: %s ", documentType));
        return sb.toString();
    }
}
