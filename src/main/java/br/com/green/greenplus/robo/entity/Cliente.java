package br.com.green.greenplus.robo.entity;

/**
 *
 * @author renato.paiva
 */
public class Cliente {
	private String id;
	private String nome;
	private String url;
	private String temCampoConta;
	private String filaProcesso;
	private String filaSucesso;
	private String filaErro;
	private String chave;
	private String sessionHash;
	private String codigoScript;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setTemCampoConta(String temCampoConta) {
		this.temCampoConta = temCampoConta;
	}

	public String getTemCampoConta() {
		return temCampoConta;
	}

	public void setFilaProcesso(String filaProcesso) {
		this.filaProcesso = filaProcesso;
	}

	public String getFilaProcesso() {
		return filaProcesso;
	}

	public void setFilaSucesso(String filaSucesso) {
		this.filaSucesso = filaSucesso;
	}

	public String getFilaSucesso() {
		return filaSucesso;
	}

	public void setFilaErro(String filaErro) {
		this.filaErro = filaErro;
	}

	public String getFilaErro() {
		return filaErro;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getChave() {
		return chave;
	}

	public String getSessionHash() {
		return sessionHash;
	}

	public void setSessionHash(String sessionHash) {
		this.sessionHash = sessionHash;
	}

	public String getCodigoScript() {
		return codigoScript;
	}

	public void setCodigoScript(String codigoScript) {
		this.codigoScript = codigoScript;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nome=" + nome + ", url=" + url + ", temCampoConta=" + temCampoConta
				+ ", filaProcesso=" + filaProcesso + ", filaSucesso=" + filaSucesso + ", filaErro=" + filaErro
				+ ", chave=" + chave + ", sessionHash=" + sessionHash + ", codigoScript=" + codigoScript + "]";
	}

}
