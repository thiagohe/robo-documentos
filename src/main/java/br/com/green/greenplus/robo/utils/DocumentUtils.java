package br.com.green.greenplus.robo.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import javax.imageio.ImageIO;
import javax.imageio.spi.IIORegistry;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

/**
 * Responsável por converter as páginas de um arquivo PDF em várias imagens TIFF
 * num folder de destino.
 *
 * @author cleberalberto
 */
public class DocumentUtils {

    public DocumentUtils() {
        IIORegistry registry = IIORegistry.getDefaultInstance();
        registry.registerServiceProvider(new com.sun.media.imageioimpl.plugins.tiff.TIFFImageReaderSpi());
        registry.registerServiceProvider(new com.sun.media.imageioimpl.plugins.tiff.TIFFImageWriterSpi());
        registry.registerServiceProvider(new com.sun.media.imageioimpl.plugins.raw.RawImageReaderSpi());
        registry.registerServiceProvider(new com.sun.media.imageioimpl.plugins.raw.RawImageWriterSpi());
        ImageIO.setUseCache(false);
    }

    public static void pdfToPNGAddPages(ByteArrayInputStream bais, String docId, String hashSessao, ImagenowUtils inUtils) throws Exception {
        PDDocument document = null;

        try {
            document = PDDocument.load(bais);
            @SuppressWarnings("unchecked")
			List<PDPage> pages = document.getDocumentCatalog().getAllPages();
            int pageNumber=2;
            for (PDPage page : pages) {
            	BufferedImage image = page.convertToImage();
            	ByteArrayOutputStream baos = new ByteArrayOutputStream();
            	ImageIO.write(image, "png", baos);
            	inUtils.inserirPagina(docId, baos.toByteArray(), pageNumber, hashSessao);
            	pageNumber++;
            }

        } finally {

            if (document != null) {
                document.close();
            }

        }
    }
    
}