package br.com.green.greenplus.robo.tipos.load;

import java.awt.Dimension;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.concurrent.Callable;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.green.greenplus.robo.entity.Cliente;
import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.entity.imagenow.Page;
import br.com.green.greenplus.robo.main.Manager;

public class ProcessJobAmericana implements Callable<Boolean> {
	private LoadTO loadTO;
	private List<OTDImagenow> documentos;
	private Cliente cliente;
	
	public ProcessJobAmericana(LoadTO loadTO, List<OTDImagenow> documentos, Cliente cliente) {
		this.loadTO = loadTO;
		this.documentos = documentos;
		this.cliente = cliente;
	}
	
	@Override
	public Boolean call() throws Exception {
		
		for (OTDImagenow indoc : documentos) {
			String filaDestino = null;
			try {
				ByteArrayOutputStream pdfBaos = new ByteArrayOutputStream();
				Document document = new Document();
				PdfWriter writer = PdfWriter.getInstance(document, pdfBaos);
				
				document.open();
				
				List<Page> pages = loadTO.getImagenowUtils().getPages(indoc.getDocId(), loadTO.getSessionHash());
				
				for (Page page : pages) {
					byte[] pagina = loadTO.getImagenowUtils().getPageBytes(indoc.getDocId(), page.getId(), loadTO.getSessionHash());
					
					Image img = Image.getInstance(pagina);
					
					Dimension boundary = new Dimension(1000, 1000);
					Dimension newDimension = calcularTamanho(new Dimension((int)img.getWidth(), (int)img.getHeight()), boundary);
					
					img.scaleAbsolute((int)newDimension.getWidth(), (int)newDimension.getHeight());
					
		            document.setPageSize(new Rectangle((int)newDimension.getWidth(), (int)newDimension.getHeight()));
		            document.newPage();
		            img.setAbsolutePosition(0, 0);
		            document.add(img);
				}
				
				document.close();
				
				byte[] pdf = pdfBaos.toByteArray();
				
				// insere pdf no imagenow.
				loadTO.getImagenowUtils().addPageDoc(indoc.getDocId(), loadTO.getSessionHash(), pdf);
				
				//deleta páginas de tif.
				for (Page page : pages) {
					loadTO.getImagenowUtils().delPageDoc(indoc.getDocId(), page.getId(), loadTO.getSessionHash());
				}
				
				pdfBaos.close();
		        writer.close();
		        
		        filaDestino = cliente.getFilaSucesso();
		        Manager.logger.info(String.format("Documento (PDF) processado com sucesso: [%s]", indoc.getDocId()));
		        
			} catch(Exception e) { 
				Manager.logger.error("["+cliente.getNome()+"] "+e.getMessage(), e);
				Manager.logger.info(String.format("Documento com erro: [%s]", indoc.getDocId()));
				filaDestino = cliente.getFilaErro();
				
			} finally {
				loadTO.getImagenowUtils().rotearDocumentoImagenow(indoc.getWorkflowItemId(), filaDestino, loadTO);
			}
			
			
			
		}
		
		return true;
	}
	
	private static Dimension calcularTamanho(Dimension imgSize, Dimension boundary) {
        int original_width = imgSize.width;
        int original_height = imgSize.height;
        int bound_width = boundary.width;
        int bound_height = boundary.height;
        int new_width = original_width;
        int new_height = original_height;

        // Verifica se precisa modificar a largura
        if (original_width > bound_width) {
            new_width = bound_width;
            new_height = (new_width * original_height) / original_width;
        }

        // Verifica se precisa modificar a altura mesmo com a nova largura
        if (new_height > bound_height) {
            new_height = bound_height;
            new_width = (new_height * original_width) / original_height;
        }

        return new Dimension(new_width, new_height);
    }
}