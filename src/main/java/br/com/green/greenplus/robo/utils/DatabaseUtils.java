package br.com.green.greenplus.robo.utils;

import br.com.green.greenplus.robo.entity.Cliente;
import br.com.green.greenplus.robo.entity.OTDDocCp;
import br.com.green.greenplus.robo.entity.OTDDocument;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.main.Manager;
import br.com.green.greenplus.robo.main.Settings;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author renato.paiva
 */
public class DatabaseUtils {

    private static Connection conGreen;
    private Cliente cliente;

    public static final String GREEN_DB_SERVER = Settings.getProperty("green.db.server");
    public static final String GREEN_DB_USER = Settings.getProperty("green.db.user");
    public static final String GREEN_DB_PASS = Settings.getProperty("green.db.pass");

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    private synchronized Connection getConnectionGreen() throws Exception {
        if (conGreen == null || conGreen.isClosed()) {
            Class.forName("oracle.jdbc.OracleDriver");
            conGreen = DriverManager.getConnection(GREEN_DB_SERVER, GREEN_DB_USER, GREEN_DB_PASS);
        }
        return conGreen;
    }

    /**
     * Consulta todos os documentos em uma fila do Imagenow.
     *
     * @param limite
     * @return List (Lista contendo o docId dos documentos que estão na fila)
     * @throws Exception
     */
    public List<OTDImagenow> documentosPorFila(int limite) throws Exception {
        List<OTDImagenow> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("doc_id, "); //1
        sb.append("folder, "); //2
        sb.append("tab, "); //3
        sb.append("f3, "); //4
        sb.append("f4, "); //5
        sb.append("f5, "); //6
        sb.append("doc_type_id, "); //7
        sb.append("doc_type_name, "); //8
        sb.append("creation_time, "); //9
        sb.append("item_id, "); //10
        sb.append("usr_name "); //11
        sb.append("FROM ");
        sb.append("(SELECT ");
        sb.append("d.doc_id, ");
        sb.append("d.folder, ");
        sb.append("d.tab, ");
        sb.append("d.f3, ");
        sb.append("d.f4, ");
        sb.append("d.f5, ");
        sb.append("d.doc_type_id, ");
        sb.append("t.doc_type_name, ");
        sb.append("w.creation_time, ");
        sb.append("w.item_id, ");
        sb.append("u.usr_name, ");
        sb.append("ROW_NUMBER() OVER (ORDER BY d.doc_id) r ");
        sb.append("FROM ");
        sb.append("inuser.in_doc d, ");
        sb.append("inuser.in_doc_type t, ");
        sb.append("inuser.in_wf_item w, ");
        sb.append("inuser.in_sc_usr u, ");
        sb.append("inuser.in_instance i ");
        sb.append("WHERE ");
        sb.append("d.doc_id = w.obj_id ");
        sb.append("AND d.doc_type_id = t.doc_type_id ");
        sb.append("AND d.instance_id = i.instance_id ");
        sb.append("AND i.creation_usr_id = u.usr_id ");
        sb.append("AND w.queue_id IN ");
        sb.append(String.format("(SELECT q.queue_id FROM inuser.in_wf_queue q WHERE q.queue_name = '%s'", cliente.getFilaProcesso()));
        sb.append(") ");
        sb.append("AND d.deletion_status = 0 ");
        sb.append(") ");
        sb.append(String.format("WHERE r <= '%s'", limite));
        try (Statement stmt = getConnectionGreen().createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sb.toString())) {
                while (rs.next()) {
                    OTDImagenow otd = new OTDImagenow();
                    otd.setDocId(rs.getString(1));
                    otd.setF1(rs.getString(2));
                    otd.setF2(rs.getString(3));
                    otd.setF3(rs.getString(4));
                    otd.setF4(rs.getString(5));
                    otd.setF5(rs.getString(6));
                    otd.setDocTypeId(rs.getString(7));
                    otd.setDocTypeName(rs.getString(8));
                    otd.setCreationDate(rs.getString(9));
                    otd.setWorkflowItemId(rs.getString(10));
                    otd.setCreator(rs.getString(11));
                    result.add(otd);
                }
            } catch (Exception e) {
            	Manager.logger.info(e.getMessage(), e);
            	throw new Exception("Erro ao consultar documentos no banco de dados do IMAGENOW");
            }
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("Erro ao consultar documentos no banco de dados do IMAGENOW");
        }
        return result;
    }
    
    public List<OTDImagenow> documentosPorFila(int limite, Cliente cliente) throws Exception {
        List<OTDImagenow> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("doc_id, "); //1
        sb.append("folder, "); //2
        sb.append("tab, "); //3
        sb.append("f3, "); //4
        sb.append("f4, "); //5
        sb.append("f5, "); //6
        sb.append("doc_type_id, "); //7
        sb.append("doc_type_name, "); //8
        sb.append("creation_time, "); //9
        sb.append("item_id, "); //10
        sb.append("usr_name "); //11
        sb.append("FROM ");
        sb.append("(SELECT ");
        sb.append("d.doc_id, ");
        sb.append("d.folder, ");
        sb.append("d.tab, ");
        sb.append("d.f3, ");
        sb.append("d.f4, ");
        sb.append("d.f5, ");
        sb.append("d.doc_type_id, ");
        sb.append("t.doc_type_name, ");
        sb.append("w.creation_time, ");
        sb.append("w.item_id, ");
        sb.append("u.usr_name, ");
        sb.append("ROW_NUMBER() OVER (ORDER BY d.doc_id) r ");
        sb.append("FROM ");
        sb.append("inuser.in_doc d, ");
        sb.append("inuser.in_doc_type t, ");
        sb.append("inuser.in_wf_item w, ");
        sb.append("inuser.in_sc_usr u, ");
        sb.append("inuser.in_instance i ");
        sb.append("WHERE ");
        sb.append("d.doc_id = w.obj_id ");
        sb.append("AND d.doc_type_id = t.doc_type_id ");
        sb.append("AND d.instance_id = i.instance_id ");
        sb.append("AND i.creation_usr_id = u.usr_id ");
        sb.append("AND w.queue_id IN ");
        sb.append(String.format("(SELECT q.queue_id FROM inuser.in_wf_queue q WHERE q.queue_name = '%s'", cliente.getFilaProcesso()));
        sb.append(") ");
        sb.append("AND d.deletion_status = 0 ");
        sb.append(") ");
        sb.append(String.format("WHERE r <= '%s'", limite));
        try (Statement stmt = getConnectionGreen().createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sb.toString())) {
                while (rs.next()) {
                    OTDImagenow otd = new OTDImagenow();
                    otd.setDocId(rs.getString(1));
                    otd.setF1(rs.getString(2));
                    otd.setF2(rs.getString(3));
                    otd.setF3(rs.getString(4));
                    otd.setF4(rs.getString(5));
                    otd.setF5(rs.getString(6));
                    otd.setDocTypeId(rs.getString(7));
                    otd.setDocTypeName(rs.getString(8));
                    otd.setCreationDate(rs.getString(9));
                    otd.setWorkflowItemId(rs.getString(10));
                    otd.setCreator(rs.getString(11));
                    result.add(otd);
                }
            } catch (Exception e) {
            	Manager.logger.info(e.getMessage(), e);
            	throw new Exception("Erro ao consultar documentos no banco de dados do IMAGENOW");
            }
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("Erro ao consultar documentos no banco de dados do IMAGENOW");
        }
        return result;
    }
    
    public List<OTDImagenow> documentosPorFilaMVContratos(int limite, Cliente cliente) throws Exception {
        List<OTDImagenow> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("doc_id, "); //1
        sb.append("folder, "); //2
        sb.append("tab, "); //3
        sb.append("f3, "); //4
        sb.append("f4, "); //5
        sb.append("f5, "); //6
        sb.append("doc_type_id, "); //7
        sb.append("doc_type_name, "); //8
        sb.append("creation_time, "); //9
        sb.append("item_id, "); //10
        sb.append("usr_name, "); //11
        sb.append("drawer_id, "); //  drawer_name
        sb.append("drawer_name ");
        sb.append("FROM ");
        sb.append("(SELECT ");
        sb.append("doc.doc_id, ");
        sb.append("doc.folder, ");
        sb.append("doc.tab, ");
        sb.append("doc.f3, ");
        sb.append("doc.f4, ");
        sb.append("doc.f5, ");
        sb.append("doc.doc_type_id, ");
        sb.append("doc.doc_type_name, ");
        sb.append("w.creation_time, ");
        sb.append("w.item_id, ");
        sb.append("u.usr_name, ");
        sb.append("d.drawer_id, ");
        sb.append("ROW_NUMBER() OVER (ORDER BY d.doc_id) r, ");
        sb.append("( SELECT DRAWER_NAME FROM INUSER.IN_DRAWER WHERE DRAWER_ID = d.drawer_id ) as drawer_name ");
        sb.append("FROM ");
        sb.append("inuser.in_doc d, ");
        sb.append("inuser.in_doc_type t, ");
        sb.append("inuser.in_wf_item w, ");
        sb.append("inuser.in_sc_usr u, ");
        sb.append("inuser.in_instance i, ");
        sb.append("gp_document doc ");
        sb.append("WHERE ");
        sb.append("d.doc_id = w.obj_id ");
        sb.append("AND d.doc_type_id = t.doc_type_id ");
        sb.append("AND d.instance_id = i.instance_id ");
        sb.append("AND i.creation_usr_id = u.usr_id ");
        sb.append("AND doc.doc_id = d.doc_id ");
        sb.append("AND w.queue_id IN ");
        sb.append(String.format("(SELECT q.queue_id FROM inuser.in_wf_queue q WHERE q.queue_name = '%s'", cliente.getFilaProcesso()));
        sb.append(") ");
        sb.append("AND d.deletion_status = 0 ");
        sb.append(") ");
        sb.append(String.format("WHERE r <= '%s'", limite));
        try (Statement stmt = getConnectionGreen().createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sb.toString())) {
                while (rs.next()) {
                    OTDImagenow otd = new OTDImagenow();
                    otd.setDocId(rs.getString("doc_id"));
                    otd.setF1(rs.getString("folder"));
                    otd.setF2(rs.getString("tab"));
                    otd.setF3(rs.getString("f3"));
                    otd.setF4(rs.getString("f4"));
                    //otd.setF5(rs.getString("f5"));
                    otd.setDocTypeId(rs.getString("doc_type_id"));
                    otd.setDocTypeName(rs.getString("doc_type_name"));
                    otd.setCreationDate(rs.getString("creation_time"));
                    otd.setWorkflowItemId(rs.getString("item_id"));
                    otd.setCreator(rs.getString("usr_name"));
                    otd.setDrawerId(rs.getString("drawer_id"));
                    otd.setDrawer(rs.getString("drawer_name"));
                    result.add(otd);
                }
            } catch (Exception e) {
            	Manager.logger.info(e.getMessage(), e);
            	throw new Exception("Erro ao consultar documentos no banco de dados do IMAGENOW");
            }
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("Erro ao consultar documentos no banco de dados do IMAGENOW");
        }
        return result;
    }

    /**
     * Consulta o Id do grupo através do nome do usuário.
     *
     * @param usuario (Usuário criador do documento)
     * @return Map (Mapa contendo os dados do cliente)
     * @throws Exception
     */
    public Map<String, String> listarDadosCliente(String usuario) throws Exception {
        Map<String, String> retorno = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("gp_group.id AS id_grupo, "); //1
        sb.append("gp_client.id AS id_cliente, "); //2
        sb.append("gp_client.nome AS nm_cliente, "); //3
        sb.append("gp_group.id_compartimento AS id_compartimento "); //4
        sb.append("FROM ");
        sb.append("gp_group, ");
        sb.append("gp_client, ");
        sb.append("gp_user_group, ");
        sb.append("gp_user ");
        sb.append("WHERE ");
        sb.append("gp_user_group.id_group = gp_group.id ");
        sb.append("AND gp_user_group.id_user = gp_user.id ");
        sb.append("AND gp_group.id_client = gp_client.id ");
        sb.append(String.format("AND LOWER(gp_user.username) = LOWER('%s') ", usuario));
        try (Statement stmt = getConnectionGreen().createStatement();
            ResultSet rs = stmt.executeQuery(sb.toString())) {
            if (rs.next()) {
            	String idGrupo = rs.getString(1);
            	String idCliente = rs.getString(2);
            	if (idGrupo == null || idCliente == null) {
					throw new Exception("Cliente/grupo não encontrados no banco de dados da GREEN");
				}
                retorno.put("grupo", idGrupo);
                retorno.put("id_cliente", idCliente);
                retorno.put("nm_cliente", rs.getString(3));
                retorno.put("compartimento", rs.getString(4));
            }
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("Erro ao consultar usuário no banco de dados do SAME");
        }
        return retorno;
    }

    /**
     *
     * @param idCompartimento (Identificador do compartimento)
     * @param idCliente (Identificador do cliente)
     * @return Map (Mapa contendo as custom properties do cliente)
     * @throws Exception
     */
    public Map<String, Integer> listarCustomProperties(String idCompartimento, String idCliente) throws Exception {
        Map<String, Integer> retorno = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append("id, "); //1
        sb.append("name "); //2
        sb.append("FROM ");
        sb.append("gp_custom_properties ");
        sb.append("WHERE ");
        sb.append(String.format("id_compartimento = '%s' ", idCompartimento));
        sb.append(String.format("AND id_client = '%s' ", idCliente));
        
        try (Statement stmt = getConnectionGreen().createStatement();
            ResultSet rs = stmt.executeQuery(sb.toString())) {
            while (rs.next()) {
                retorno.put(rs.getString(2), rs.getInt(1));
            }
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("[LEFORTE] Propriedades não cadastradas no portal do SAME");
        }
        return retorno;
    }

    /**
     * Cria o comando INSERT que vai inserir o documento na tabela TB_DOCUMENT.
     *
     * @param docId (Id do documento no Imagenow)
     * @throws Exception
     */
    public void removerGpDocument(String docId) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(" DELETE FROM gp_document WHERE doc_id = '%s'", docId));
        try (Statement stmt = getConnectionGreen().createStatement()) {
            stmt.executeUpdate(sb.toString());
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("Erro ao remover documento do banco de dados do SAME");
        }
    }

    /**
     * Cria o comando INSERT que vai inserir o documento na tabela GP_DOCUMENT.
     *
     * @param otd (Objeto de transferência de dados contendo as informações
     * necessárias para fazer o INSERT)
     * @return String (Comando INSERT da tabela GP_DOCUMENT)
     * @throws Exception
     */
    public String inserirGpDocument(OTDDocument otd) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO gp_document ");
        sb.append("(");
        sb.append("id, ");
        sb.append("id_group, ");
        sb.append("doc_id, ");
        sb.append("folder, ");
        sb.append("tab, ");
        sb.append("f3, ");
        sb.append("f4, ");
        sb.append("f5, ");
        sb.append("doc_type_id, ");
        sb.append("doc_type_name, ");
        sb.append("creation_date, ");
        sb.append("creator, ");
        sb.append("sub_type_name, ");
        sb.append("deletado ");
        sb.append(") ");
        sb.append("VALUES ");
        sb.append("(");
        sb.append("v_document_id, "); //1
        sb.append(String.format("'%s', ", otd.getIdGroup())); //2
        sb.append(String.format("'%s', ", otd.getDocId())); //3
        sb.append(String.format("'%s', ", otd.getFolder())); //4
        sb.append(String.format("'%s', ", otd.getTab())); //5
        sb.append(String.format("'%s', ", otd.getF3())); //6
        sb.append(String.format("'%s', ", otd.getF4())); //7
        sb.append(String.format("'%s', ", otd.getF5())); //8
        sb.append(String.format("'%s', ", otd.getDocTypeId())); //9
        sb.append(String.format("'%s', ", otd.getDocTypeName())); //10
        sb.append(String.format("'%s', ", otd.getCreationDate())); //11
        sb.append(String.format("'%s', ", otd.getCreator())); //12
        sb.append(String.format("'%s' ", otd.getSubTypeName())); //13
        sb.append(", 'N'); ");
        return sb.toString();
    }

    /**
     * Cria o comando INSERT que vai inserir o documento na tabela GP_DOC_CP.
     *
     * @param otd (Objeto de transferência de dados contendo as informações
     * necessárias para fazer o INSERT)
     * @return String (Comando INSERT da tabela GP_DOC_CP)
     * @throws Exception
     */
    public String inserirGpDocCp(OTDDocCp otd) throws Exception {
    	StringBuilder sb = new StringBuilder(" ");
    	if (otd!=null && otd.getValue()!=null && !otd.getValue().isEmpty()) {
            sb.append("INSERT ");
            sb.append("INTO gp_doc_cp ");
            sb.append("( ");
            sb.append("id, ");
            sb.append("doc_id, ");
            sb.append("cp_id, ");
            sb.append("valor ");
            sb.append(") ");
            sb.append("VALUES ");
            sb.append("(");
            sb.append("seq_gp_doc_cp.NEXTVAL, "); //1
            sb.append("v_document_id, "); //2
            sb.append(String.format("'%s', ", otd.getCpId())); //3
            sb.append(String.format("'%s' ", otd.getValue())); //4
            sb.append("); ");
    	}
        
        return sb.toString();
    }

    /**
     * Executa um comando SQL.
     *
     * @param sql (Comando SQL que será executado)
     * @throws Exception
     */
    public void executar(StringBuilder sql) throws Exception {
        try (Statement stmt = getConnectionGreen().createStatement()) {
            stmt.executeUpdate(sql.toString());
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("Erro ao executar inserts no banco de dados do SAME");
        }
    }

    /**
     * Consulta o Id do da fila do workflow através do nome dela.
     *
     * @param fila (Nome da fila)
     * @return String (Id da fila)
     * @throws Exception
     */
    public String listarFilaPorNome(String fila) throws Exception {
        String retorno = null;
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("SELECT queue_id FROM inuser.in_wf_queue WHERE queue_name = '%s'", fila));
        try (Statement stmt = getConnectionGreen().createStatement();
            ResultSet rs = stmt.executeQuery(sb.toString())) {
            if (rs.next()) {
                retorno = rs.getString(1);
            }
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception("Erro ao obter nome da fila no banco de dados do IMAGENOW");
        }
        return retorno;
    }
    
    public void adicionarIncidente(long cdTipoIncidente, String stackTrace, long idCliente) {
        try {
            LocalDate localDate = LocalDate.now();
            String sql = "INSERT INTO GP_NOT_INCIDENTE(CD_INCIDENTE, CD_TIPO_INCIDENTE, DH_INCIDENTE, NM_STACK, ID_CLIENT) "
                    + "VALUES (SEQ_GP_NOT_INCIDENTE.NEXTVAL, "+cdTipoIncidente+", "
                    + "TO_DATE('"+localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+"', 'dd/MM/yyyy'), "
                    + "'"+stackTrace+"', "+idCliente+" )";
            
            try (Statement stmt = getConnectionGreen().createStatement()) {
                stmt.execute(sql);
            }
            
        } catch (Exception e) {
        	Manager.logger.info("Falha ao adicionar incidente");
        }
    }
    
    /**
     * 
     * @param valor
     * @return 
     * @throws java.lang.Exception 
     */
    public String atualizarF2GpDocument(String valor) throws Exception {
    	return " UPDATE GP_DOCUMENT SET TAB = '"+valor+"' WHERE ID = v_document_id; ";
    }
}
