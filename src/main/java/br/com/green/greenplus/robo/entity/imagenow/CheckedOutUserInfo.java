package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class CheckedOutUserInfo {

    private String userId;
    private String userName;

    public String getUserId() {return (userId == null ? "" : userId);}
    public void setUserId(String userId) {this.userId = userId;}

    public String getUserName() {return (userName == null ? "" : userName);}
    public void setUserName(String userName) {this.userName = userName;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("userId: %s ", userId));
        sb.append(String.format("userName: %s ", userName));
        return sb.toString();
    }
}
