package br.com.green.greenplus.robo.entity;

/**
 *
 * @author renato.paiva
 */
public class OTDImagenow {

    private String docId;
    private String f1;
    private String f2;
    private String f3;
    private String f4;
    private String f5;
    private String docTypeId;
    private String docTypeName;
    private String creationDate;
    private String workflowItemId;
    private String creator;
    private String notes;
    private String drawerId;
    private String drawer;

    public String getDrawerId() {
		return (drawerId == null ? "" : drawerId);
	}
	public void setDrawerId(String drawerId) {
		this.drawerId = drawerId;
	}
	public String getDrawer() {
		return (drawer == null ? "" : drawer);
	}
	public void setDrawer(String drawer) {
		this.drawer = drawer;
	}
	
	public String getDocId() {return (docId == null ? "" : docId);}
    public void setDocId(String docId) {this.docId = docId;}

    public String getF1() {return (f1 == null ? "" : f1);}
    public void setF1(String f1) {this.f1 = f1;}

    public String getF2() {return (f2 == null ? "" : f2);}
    public void setF2(String f2) {this.f2 = f2;}

    public String getF3() {return (f3 == null ? "" : f3);}
    public void setF3(String f3) {this.f3 = f3;}

    public String getF4() {return (f4 == null ? "" : f4);}
    public void setF4(String f4) {this.f4 = f4;}

    public String getF5() {return (f5 == null ? "" : f5);}
    public void setF5(String f5) {this.f5 = f5;}

    public String getDocTypeId() {return (docTypeId == null ? "" : docTypeId);}
    public void setDocTypeId(String docTypeId) {this.docTypeId = docTypeId;}

    public String getDocTypeName() {return (docTypeName == null ? "" : docTypeName);}
    public void setDocTypeName(String docTypeName) {this.docTypeName = docTypeName;}

    public String getCreationDate() {return (creationDate == null ? "" : creationDate);}
    public void setCreationDate(String creationDate) {this.creationDate = creationDate;}

    public String getWorkflowItemId() {return (workflowItemId == null ? "" : workflowItemId);}
    public void setWorkflowItemId(String workflowItemId) {this.workflowItemId = workflowItemId;}

    public String getCreator() {return (creator == null ? "" : creator);}
    public void setCreator(String creator) {this.creator = creator;}

    public String getNotes() {return (notes == null ? "" : notes);}
    public void setNotes(String notes) {this.notes = notes;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("docId: %s ", docId));
        sb.append(String.format("f1: %s ", f1));
        sb.append(String.format("f2: %s ", f2));
        sb.append(String.format("f3: %s ", f3));
        sb.append(String.format("f4: %s ", f4));
        sb.append(String.format("f5: %s ", f5));
        sb.append(String.format("docTypeId: %s ", docTypeId));
        sb.append(String.format("docTypeName: %s ", docTypeName));
        sb.append(String.format("creationDate: %s ", creationDate));
        sb.append(String.format("workflowItemId: %s ", workflowItemId));
        sb.append(String.format("creator: %s ", creator));
        sb.append(String.format("notes: %s ", notes));
        return sb.toString();
    }
}
