package br.com.green.greenplus.robo.utils;

public class CMPWebServiceException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8348413631576141243L;

	public CMPWebServiceException(String msg) {
        super(msg);
    }
}