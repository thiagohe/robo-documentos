package br.com.green.greenplus.robo.main;

import br.com.green.greenplus.robo.entity.Cliente;
import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.threads.exclusivas.MvContratosThread;
import br.com.green.greenplus.robo.threads.exclusivas.UnimedAmericanaThread;
import br.com.green.greenplus.robo.tipos.load.ProcessJobIPPMG;
import br.com.green.greenplus.robo.tipos.load.ProcessJobCMP;
import br.com.green.greenplus.robo.tipos.load.ProcessJobLeforte;
import br.com.green.greenplus.robo.utils.DatabaseUtils;
import br.com.green.greenplus.robo.utils.ImagenowUtils;
import br.com.green.greenplus.robo.utils.JsonUtils;
import br.com.green.greenplus.robo.utils.WebserviceUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;

/**
 *
 * @author renato.paiva
 */
public class Manager {

    public static final Logger logger = Logger.getLogger("Informacoes");
    private static List<Cliente> clientes;
    private static List<Cliente> clientesExclusivos;
    private static Cliente cliente;
    private static int indiceClientes = -1;
    public static int maxDocumentosFila = Integer.parseInt(Settings.getProperty("green.max.documentos.fila"));
    public static int limiteProcessos = Integer.parseInt(Settings.getProperty("green.limite.processos"));

    /**
     * Inicia a execução do programa.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        obterClientesAExecutar();
        if (clientes.isEmpty() && clientesExclusivos.isEmpty()) {
        	Manager.logger.error("Encerrando execucao por falta de conexoes de clientes.");
            System.exit(0);
        } 
        
        // início de clientes que precisam de threads dedicadas
        for (Cliente clienteExclusivo : clientesExclusivos) {
        	LoadTO loadTO = new LoadTO();

        	loadTO.setDatabaseUtils(new DatabaseUtils());
            loadTO.setImagenowUtils(new ImagenowUtils());
            loadTO.setWebserviceUtils(new WebserviceUtils());
            loadTO.setJsonUtils(new JsonUtils());
            
            loadTO.setCliente(clienteExclusivo);
        	loadTO.getDatabaseUtils().setCliente(clienteExclusivo);
            loadTO.setSessionHash(loadTO.getImagenowUtils().obterSessionHash(loadTO.getSessionHash()));
            
            if (clienteExclusivo.getCodigoScript().equals("AMERICANA")) {
            	new UnimedAmericanaThread(clienteExclusivo, loadTO).start();
            }
            else if (clienteExclusivo.getCodigoScript().equals("MV_CONTRATOS")) {
            	new MvContratosThread(clienteExclusivo, loadTO).start();
            }
        }
        // fim threads dedicadas
        
        LoadTO loadTO = new LoadTO();
        loadTO.setDatabaseUtils(new DatabaseUtils());
        loadTO.setImagenowUtils(new ImagenowUtils());
        loadTO.setWebserviceUtils(new WebserviceUtils());
        loadTO.setJsonUtils(new JsonUtils());
        
        Manager.logger.info(clientes.size() + " cliente(s) em processamento circular.");
        // threads em processamento circular (quantum)
        new Manager().execute(loadTO);
    }

    private void execute(LoadTO loadTO) {

        while (!clientes.isEmpty()) {
            try {
                cliente = obterProximoCliente();

                loadTO.getDatabaseUtils().setCliente(cliente);
                loadTO.setCliente(cliente);
                loadTO.setSessionHash(loadTO.getImagenowUtils().obterSessionHash(loadTO.getSessionHash()));

                List<OTDImagenow> documentos = loadTO.getDatabaseUtils().documentosPorFila(maxDocumentosFila);
                if (documentos.isEmpty()) {
                    Thread.sleep(10000);
                } else {
                    logger.info(documentos.size() + " documento(s) obtido(s) na fila [" + cliente.getFilaProcesso() + "]. Iniciando Threads.");

                    if (cliente.getCodigoScript().equals("IPPMG")) {
                        List<ProcessJobIPPMG> jobs = new ArrayList<>();

                        for (List<OTDImagenow> sublista : dividir(documentos, limiteProcessos)) {
                            jobs.add(new ProcessJobIPPMG(loadTO, sublista));
                        }

                        ExecutorService executor = Executors.newFixedThreadPool(limiteProcessos);
                        executor.invokeAll(jobs);
                        executor.shutdown();
                    } else if (cliente.getCodigoScript().equals("LEFORTE")) {
                        List<ProcessJobLeforte> jobs = new ArrayList<>();

                        for (List<OTDImagenow> sublista : dividir(documentos, limiteProcessos)) {
                            jobs.add(new ProcessJobLeforte(loadTO, sublista));
                        }

                        ExecutorService executor = Executors.newFixedThreadPool(limiteProcessos);
                        executor.invokeAll(jobs);
                        executor.shutdown();
                    } else if (cliente.getCodigoScript().equals("CMP")) {
                        List<ProcessJobCMP> jobs = new ArrayList<>();

                        for (List<OTDImagenow> sublista : dividir(documentos, limiteProcessos)) {
                            jobs.add(new ProcessJobCMP(loadTO, sublista));
                        }

                        ExecutorService executor = Executors.newFixedThreadPool(limiteProcessos);
                        executor.invokeAll(jobs);
                        executor.shutdown();
                    }
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e1) {
                    logger.error(e1.getMessage(), e1);
                }
            }
        }
    }

    /**
     * Cria a lista de sub-listas a partir de uma lista maior.
     *
     * @param <T> (Classe template)
     * @param lista (Lista maior de documentos que vai ser quebrada em
     * sub-listas)
     * @param limite (Tamanho máximo de cada sub-lista)
     * @return List<List<>> (Lista contendo uma ou mais sub-listas de
     * documentos)
     */
    public static <T> List<List<T>> dividir(List<T> lista, final int limite) {
        List<List<T>> parts = new ArrayList<>();
        final int TAMANHO = lista.size();
        for (int i = 0; i < TAMANHO; i += limite) {
            parts.add(new ArrayList<>(lista.subList(i, Math.min(TAMANHO, i + limite))));
        }
        return parts;
    }

    /**
     * Carrega na memória as informações dos clientes que terão as assinaturas
     * coletadas. (Multiempresa)
     *
     * @throws java.lang.Exception
     */
    public static void obterClientesAExecutar() throws Exception {
        Class.forName("oracle.jdbc.OracleDriver");
        clientes = new ArrayList<>();
        clientesExclusivos = new ArrayList<>();
        
        try (Connection connection = DriverManager.getConnection(DatabaseUtils.GREEN_DB_SERVER,
            DatabaseUtils.GREEN_DB_USER, DatabaseUtils.GREEN_DB_PASS)) {
            try (Statement stmt = connection.createStatement()) {
                String sql = "SELECT ID_CLIENT, URL, TEM_CAMPO_CONTA, FILA_PROCESSO, FILA_SUCESSO, FILA_ERRO, CHAVE, CODIGO_SCRIPT, EXCLUSIVO FROM GP_WEBSERVICE "
                		+ "WHERE CODIGO_SCRIPT IS NOT NULL";
                try (ResultSet rs = stmt.executeQuery(sql)) {
                    while (rs.next()) {
                        Cliente cliente = new Cliente();
                        cliente.setId(rs.getString("ID_CLIENT"));
                        cliente.setUrl(rs.getString("URL"));
                        cliente.setTemCampoConta(rs.getString("TEM_CAMPO_CONTA"));
                        cliente.setFilaProcesso(rs.getString("FILA_PROCESSO"));
                        cliente.setFilaSucesso(rs.getString("FILA_SUCESSO"));
                        cliente.setFilaErro(rs.getString("FILA_ERRO"));
                        cliente.setChave(rs.getString("CHAVE"));
                        cliente.setCodigoScript(rs.getString("CODIGO_SCRIPT"));
                        cliente.setNome(rs.getString("CODIGO_SCRIPT"));
                        
                        if (rs.getInt("EXCLUSIVO")==0) {
                        	clientes.add(cliente);
                        }
                        else {
                        	clientesExclusivos.add(cliente);
                        }
                        
                    }
                }
            }
        }
    }
    

    /**
     * Carrega na memória as informações dos clientes que terão as assinaturas
     * coletadas. (Multiempresa). Funcionamento como lista circular.
     *
     * @return Cliente (Próximo cliente a ser processado)
     */
    private Cliente obterProximoCliente() {
        if (indiceClientes < clientes.size() - 1) {
            indiceClientes++;
        } else {
            indiceClientes = 0;
        }
        return clientes.get(indiceClientes);
    }
}
