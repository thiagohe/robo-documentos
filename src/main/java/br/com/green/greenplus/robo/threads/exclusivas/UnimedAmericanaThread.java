package br.com.green.greenplus.robo.threads.exclusivas;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import br.com.green.greenplus.robo.entity.Cliente;
import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.main.Manager;
import br.com.green.greenplus.robo.tipos.load.ProcessJobAmericana;

public class UnimedAmericanaThread extends Thread {

	private Cliente cliente;
	private LoadTO loadTO;

	public UnimedAmericanaThread(Cliente cliente, LoadTO loadTO) {
		this.cliente = cliente;
		this.loadTO = loadTO;
	}

	public void run() {
		Manager.logger.info("Thread dedicada para Unimed Americana iniciada.");
		while (true) {
			try {
				loadTO.setSessionHash(loadTO.getImagenowUtils().obterSessionHash(loadTO.getSessionHash()));
				List<OTDImagenow> documentos = loadTO.getDatabaseUtils().documentosPorFila(Manager.maxDocumentosFila, cliente);
				if (documentos.isEmpty()) {
					Thread.sleep(5000);
				} else {
					List<ProcessJobAmericana> jobs = new ArrayList<>();

					for (List<OTDImagenow> sublista : Manager.dividir(documentos, Manager.limiteProcessos)) {
						jobs.add(new ProcessJobAmericana(loadTO, sublista, cliente));
					}

					ExecutorService executor = Executors.newFixedThreadPool(Manager.limiteProcessos);
					executor.invokeAll(jobs);
					executor.shutdown();
				}
			} catch (Exception e) {
				Manager.logger.error(e.getMessage(), e);
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					Manager.logger.error(e1.getMessage(), e1);
				}
			}
		}
	}
}