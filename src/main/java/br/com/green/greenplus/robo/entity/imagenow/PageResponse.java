package br.com.green.greenplus.robo.entity.imagenow;

import java.util.List;

public class PageResponse {
    private List<Page> pages;

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

}