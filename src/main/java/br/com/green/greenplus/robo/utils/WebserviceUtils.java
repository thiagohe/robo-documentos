package br.com.green.greenplus.robo.utils;

import br.com.green.greenplus.robo.entity.Cliente;
import br.com.green.greenplus.robo.entity.ODTAtendimentoAux;
import br.com.green.greenplus.robo.entity.OTDAtendimento;
import br.com.green.greenplus.robo.entity.OTDAtendimentoLeforte;
import br.com.green.greenplus.robo.entity.OTDPeriodoConta;
import br.com.green.greenplus.robo.entity.OTDPeriodoContaAux;
import br.com.green.greenplus.robo.main.Manager;
import br.com.green.greenplus.robo.main.Settings;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author renato.paiva
 */
public class WebserviceUtils {

	private final int sucesso = 200;
	private final int TIMEOUT = 5000;
	private static final String URL_SERVICO_CMP = Settings.getProperty("web.service.cmp.url");
	private static final String URL_SERVICO_CMP_PATH = Settings.getProperty("web.service.cmp.path");
	private static final String URI_SERVICO_CMP_PATH = Settings.getProperty("web.service.cmp.uri");

	/**
	 * Abre uma conexão com o Imagenow via integration server.
	 *
	 * @param cdAtendimento
	 *            (Código do atendimento)
	 * @param cdPaciente
	 *            (Dados de configuração do cliente)
	 * @param cliente
	 * @return String (String de conexão)
	 * @throws Exception
	 */
	public OTDAtendimento obterAtendimento(String cdAtendimento, String cdPaciente, Cliente cliente) throws Exception {
		OTDAtendimento retorno = null;

		String url = cliente.getUrl() + "/obterAtendimento?";
		if (cdAtendimento != null) {
			url += String.format("codigoAtendimento=%s", cdAtendimento);
		} else if (cdPaciente != null) {
			url += String.format("codigoPaciente=%s", cdPaciente);
		} else {
			throw new Exception("Requisição não possui informações suficientes");
		}
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT)
				.build();

		String mensagemErro = "[IPPMG] Erro ao chamar WebService";

		try (CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig)
				.build()) {
			HttpGet httpGet = new HttpGet(url);
			httpGet.addHeader("accept", "application/json");

			try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
				if (response.getStatusLine().getStatusCode() == sucesso) {
					String str = EntityUtils.toString(response.getEntity(), "UTF-8");
					Gson gson = new GsonBuilder().create();

					retorno = gson.fromJson(str, ODTAtendimentoAux.class).getAtendimento();

					/*
					 * Caso a consulta seja pelo paciente, seta as propriedades
					 * não-utilizadas para null
					 */
					if (cdAtendimento == null) {
						retorno.setCdAtendimento(null);
						retorno.setCdCid(null);
						retorno.setCdConPlan(null);
						retorno.setCdConvenio(null);
						retorno.setCdProcedimento(null);
						retorno.setCdSubPlano(null);
						retorno.setDsConPla(null);
						retorno.setHrAtendimento(null);
						retorno.setNmConvenio(null);
						retorno.setNmPrestador(null);
						retorno.setNmResponsavel(null);
						retorno.setNrCarteira(null);
						retorno.setNrGuia(null);
						retorno.setSenhaSus(null);
						retorno.setTpAtendimento(null);
					}
				} else {
					mensagemErro = String.format(
							"[IPPMG] Não foi possível consultar o atendimento no webservice da MV [Atendimento: %s - HTTP code: %s]",
							cdAtendimento, response.getStatusLine().getStatusCode());
					throw new Exception();
				}
			}
		} catch (Exception e) {
			Manager.logger.info(e.getMessage(), e);
			throw new Exception(mensagemErro);
		}
		return retorno;
	}

	public OTDAtendimento obterAtendimentoCMP(String cdPaciente, long idCliente, DatabaseUtils databaseUtils) throws Exception {
		int tentativas = 5;
		int indice = 0;
		OTDAtendimento OTDAtendimento = null;
		
		boolean isCheckedException = false;
		String msg = null;

		while (indice < tentativas) {
			try {
				OTDAtendimento = obterAtendimentoCMPInterno(cdPaciente);
				break;
			} catch (Exception e) {
				if (!isCheckedException) {
					isCheckedException = e instanceof CMPWebServiceException || e instanceof SOAPException;
					if (msg==null) {
						msg = e.getMessage();
					}
				}
				indice++;
				Manager.logger.info("[CMP] WebService: erro ao chamar serviço. Tentativa número " + indice);
				Thread.sleep(500);
			}
		}
		
		if (isCheckedException) {
			databaseUtils.adicionarIncidente(4, msg, idCliente);
		}

		return OTDAtendimento;
	}

	private OTDAtendimento obterAtendimentoCMPInterno(String cdPaciente) throws CMPWebServiceException,SOAPException,Exception {
    	OTDAtendimento atendimento = new OTDAtendimento();
    	String respostaXML = null;
    	
    	try {
            String xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='" + URI_SERVICO_CMP_PATH + "'> <soapenv:Header/> <soapenv:Body> <web:Consultar_paciente> <web:numH>" + cdPaciente + "</web:numH> </web:Consultar_paciente> </soapenv:Body> </soapenv:Envelope>";

            SOAPMessage message = getSoapMessageFromString(xml);

            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection connection = soapConnectionFactory.createConnection();
            URL endpoint = new URL(new URL(URL_SERVICO_CMP), URL_SERVICO_CMP_PATH,
                new URLStreamHandler() {
                @Override
                protected URLConnection openConnection(URL url) throws IOException {
                    URL target = new URL(url.toString());
                    URLConnection connection = target.openConnection();
                    // Connection settings
                    connection.setConnectTimeout(20000); // 20 sec
                    connection.setReadTimeout(60000); // 1 min
                    return (connection);
                }
            });

            SOAPMessage soapResponse = connection.call(message, endpoint);

            final StringWriter sw = new StringWriter();
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(soapResponse.getSOAPPart()),
                new StreamResult(sw));

            connection.close();

            respostaXML = sw.toString();

            String conteudoConsultaPaciente = respostaXML.split("<Consultar_pacienteResult>")[1]
                .split("</Consultar_pacienteResult>")[0];

            try {
                String pachis = conteudoConsultaPaciente.split("<pachis>")[1].split("</pachis>")[0];
                atendimento.setCdPaciente(pachis);
            } catch (ArrayIndexOutOfBoundsException ee) {
            }

            try {
                String nomPac = conteudoConsultaPaciente.split("<nom_pac>")[1].split("</nom_pac>")[0];
                atendimento.setNmPaciente(nomPac);
            } catch (ArrayIndexOutOfBoundsException ee) {
            }
            try {
                String cedula = conteudoConsultaPaciente.split("<cedula>")[1].split("</cedula>")[0];
                if (cedula != null && !cedula.isEmpty()) {
                    cedula = cedula.replaceAll("\\(null\\)", "");
                }
                atendimento.setNrIdentidade(cedula);
            } catch (ArrayIndexOutOfBoundsException ee) {
            }
            try {
                String fecNac = conteudoConsultaPaciente.split("<fec_nac>")[1].split("</fec_nac>")[0];
                if (fecNac != null && !fecNac.isEmpty()) {
                    fecNac = fecNac.replaceAll("\\(null\\)", "");
                }
                atendimento.setDtNascimento(fecNac);
            } catch (ArrayIndexOutOfBoundsException ee) {
            }
            try {
                String pacNombreMadre = conteudoConsultaPaciente.split("<pac_nombre_madre>")[1]
                    .split("</pac_nombre_madre>")[0];
                if (pacNombreMadre != null && !pacNombreMadre.isEmpty()) {
                    pacNombreMadre = pacNombreMadre.replaceAll("\\(null\\)", "");
                }
                atendimento.setNmMae(pacNombreMadre);
            } catch (ArrayIndexOutOfBoundsException ee) {
            }
            try {
                String exception = conteudoConsultaPaciente.split("<excepcion>")[1].split("</excepcion>")[0];

                if (exception != null && !exception.isEmpty()) {
                    throw new Exception(exception);
                }
            } catch (ArrayIndexOutOfBoundsException ee) {
            }
    	} catch (CMPWebServiceException cwe) {
            if (respostaXML!=null && !respostaXML.isEmpty()) {
                throw new CMPWebServiceException(respostaXML);
            }
            else {
                throw new CMPWebServiceException(cwe.getMessage());
            }
        } catch (SOAPException se) {
            if (respostaXML!=null && !respostaXML.isEmpty()) {
                throw new SOAPException(respostaXML);
            }
            else {
                throw new SOAPException("Timeout. "+se.getMessage());
            }
        }

        return atendimento;
    }

	private SOAPMessage getSoapMessageFromString(String xml) throws Exception {
		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage(new MimeHeaders(),
				new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8"))));
		return message;
	}

	/**
	 * Abre uma conexão com o Imagenow via integration server.
	 *
	 * @param cdAtendimento
	 *            (Código do atendimento)
	 * @param nrConta
	 *            (Número da conta do atendimento)
	 * @param cliente
	 *            (Dados de configuração do cliente)
	 * @return String (String de conexão)
	 * @throws Exception
	 */
	public OTDPeriodoConta obterPeriodoConta(String cdAtendimento, String nrConta, Cliente cliente) throws Exception {
		OTDPeriodoContaAux retorno = null;
		String url = cliente.getUrl() + "/obterPeriodoConta?";
		if (cdAtendimento != null && nrConta != null) {
			url += String.format("codigoAtendimento=%s", cdAtendimento);
			url += String.format("&numeroConta=%s", nrConta);
		} else {
			throw new Exception("Requisição não possui informações suficientes");
		}
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT)
				.build();

		String mensagemErro = "[IPPMG] Erro ao chamar WebService";

		try (CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig)
				.build()) {
			HttpGet httpGet = new HttpGet(url);
			httpGet.addHeader("accept", "application/json");

			try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
				if (response.getStatusLine().getStatusCode() == sucesso) {
					String str = EntityUtils.toString(response.getEntity(), "UTF-8");
					Gson gson = new GsonBuilder().create();
					retorno = gson.fromJson(str, OTDPeriodoContaAux.class);
				} else {
					mensagemErro = "[IPPMG] Não foi possível consultar o período da conta no webservice da MV";
					throw new Exception();
				}
			}
		} catch (Exception e) {
			Manager.logger.info(e.getMessage(), e);
			throw new Exception(mensagemErro);
		}
		return retorno.getPeriodoConta();
	}

	public OTDAtendimentoLeforte listarAtendimentoLeforte(String atendimento, Cliente cliente) throws Exception {
		OTDAtendimentoLeforte retorno = null;
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT)
				.build();

		String mensagemErro = "[LEFORTE] Erro ao chamar WebService";

		try (CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig)
				.build()) {
			HttpPost httpPost = new HttpPost(cliente.getUrl());
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

			List<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("CHAVE", cliente.getChave()));
			urlParameters.add(new BasicNameValuePair("CD_ATENDIMENTO", atendimento));
			HttpEntity postParams = new UrlEncodedFormEntity(urlParameters);
			httpPost.setEntity(postParams);

			try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
				if (response.getStatusLine().getStatusCode() == sucesso) {
					String str = EntityUtils.toString(response.getEntity(), "UTF-8");
					Gson gson = new GsonBuilder().create();
					OTDAtendimentoLeforte[] lista = gson.fromJson(str, OTDAtendimentoLeforte[].class);
					if (lista.length == 0) {
						mensagemErro = "[LEFORTE] Atendimento não encontrado no webservice da MV";
						throw new Exception();
					}
					retorno = lista[0];
				} else {
					mensagemErro = String.format(
							"[LEFORTE] Não foi possível consultar o atendimento no webservice da MV [Atendimento: %s - HTTP code: %s]",
							atendimento, response.getStatusLine().getStatusCode());
					throw new Exception();
				}
			}
		} catch (Exception e) {
			Manager.logger.info(e.getMessage(), e);
			throw new Exception(mensagemErro);
		}
		return retorno;
	}

	/**
	 * Abre uma conexão com o Imagenow via integration server.
	 *
	 * @param paciente
	 *            (Código do paciente)
	 * @param cliente
	 *            (Dados de configuração do cliente)
	 * @return String (String de conexão)
	 * @throws Exception
	 */
	public OTDAtendimentoLeforte listarPacienteLeforte(String paciente, Cliente cliente) throws Exception {
		OTDAtendimentoLeforte retorno = null;
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT)
				.build();

		String mensagemErro = "[LEFORTE] Erro ao chamar WebService";

		try (CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig)
				.build()) {
			HttpPost httpPost = new HttpPost(cliente.getUrl());
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

			List<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("CHAVE", cliente.getChave()));
			urlParameters.add(new BasicNameValuePair("CD_PACIENTE", paciente));
			HttpEntity postParams = new UrlEncodedFormEntity(urlParameters);
			httpPost.setEntity(postParams);

			try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
				if (response.getStatusLine().getStatusCode() == sucesso) {
					String str = EntityUtils.toString(response.getEntity(), "UTF-8");
					Gson gson = new GsonBuilder().create();
					OTDAtendimentoLeforte[] lista = gson.fromJson(str, OTDAtendimentoLeforte[].class);
					if (lista.length == 0) {
						mensagemErro = "[LEFORTE] Paciente não encontrado no webservice da MV";
						throw new Exception();
					}
					retorno = lista[0];
				} else {
					mensagemErro = String.format(
							"[LEFORTE] Não foi possível consultar o paciente no webservice da MV [Paciente: %s - HTTP code: %s]",
							paciente, response.getStatusLine().getStatusCode());
					throw new Exception();
				}
			}
		} catch (Exception e) {
			Manager.logger.info(e.getMessage(), e);
			throw new Exception(mensagemErro);
		}
		return retorno;
	}

	/**
	 * Abre uma conexão com o Imagenow via integration server.
	 *
	 * @param cdAtendimento
	 *            (Código do atendimento)
	 * @param nrConta
	 *            (Número da conta do atendimento)
	 * @param cliente
	 *            (Dados de configuração do cliente)
	 * @return String (String de conexão)
	 * @throws Exception
	 */
	public OTDPeriodoConta listarPeriodoContaLeforte(String cdAtendimento, String nrConta, Cliente cliente)
			throws Exception {
		OTDPeriodoConta retorno = null;
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT)
				.build();

		String mensagemErro = "[LEFORTE] Erro ao chamar WebService";

		try (CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig)
				.build()) {
			HttpPost httpPost = new HttpPost(cliente.getUrl());
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

			List<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("CHAVE", cliente.getChave()));
			urlParameters.add(new BasicNameValuePair("CD_ATENDIMENTO", cdAtendimento));
			urlParameters.add(new BasicNameValuePair("CD_CONTA", nrConta));
			HttpEntity postParams = new UrlEncodedFormEntity(urlParameters);
			httpPost.setEntity(postParams);

			try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
				if (response.getStatusLine().getStatusCode() == sucesso) {
					String str = EntityUtils.toString(response.getEntity(), "UTF-8");
					Gson gson = new GsonBuilder().create();
					OTDPeriodoConta[] lista = gson.fromJson(str, OTDPeriodoConta[].class);
					if (lista.length == 0) {
						mensagemErro = "[LEFORTE] Período da conta não encontrado no webservice da MV";
						throw new Exception();
					}
					retorno = lista[0];
				} else {
					mensagemErro = "[LEFORTE] Não foi possível consultar o período da conta no webservice da MV";
					throw new Exception();
				}
			}
		} catch (Exception e) {
			Manager.logger.info(e.getMessage(), e);
			throw new Exception(mensagemErro);
		}
		return retorno;
	}
}
