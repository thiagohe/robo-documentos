package br.com.green.greenplus.robo.entity;

/**
 *
 * @author renato.paiva
 */
public class OTDDocument {

    private String idGroup;
    private String docId;
    private String folder;
    private String tab;
    private String f3;
    private String f4;
    private String f5;
    private String docTypeId;
    private String docTypeName;
    private String creationDate;
    private String creator;
    
    private String subTypeName;
    private String setor;
    
    public String getSetor() {return (setor == null ? "" : setor);}
    public void setSetor(String setor) {this.setor = setor;}

    public String getIdGroup() {return (idGroup == null ? "" : idGroup);}
    public void setIdGroup(String idGroup) {this.idGroup = idGroup;}

    public String getDocId() {return (docId == null ? "" : docId);}
    public void setDocId(String docId) {this.docId = docId;}

    public String getFolder() {return (folder == null ? "" : folder);}
    public void setFolder(String folder) {this.folder = folder;}

    public String getTab() {return (tab == null ? "" : tab);}
    public void setTab(String tab) {this.tab = tab;}

    public String getF3() {return (f3 == null ? "" : f3);}
    public void setF3(String f3) {this.f3 = f3;}

    public String getF4() {return (f4 == null ? "" : f4);}
    public void setF4(String f4) {this.f4 = f4;}

    public String getF5() {return (f5 == null ? "" : f5);}
    public void setF5(String f5) {this.f5 = f5;}

    public String getDocTypeId() {return (docTypeId == null ? "" : docTypeId);}
    public void setDocTypeId(String docTypeId) {this.docTypeId = docTypeId;}

    public String getDocTypeName() {return (docTypeName == null ? "" : docTypeName);}
    public void setDocTypeName(String docTypeName) {this.docTypeName = docTypeName;}

    public String getCreationDate() {return (creationDate == null ? "" : creationDate);}
    public void setCreationDate(String creationDate) {this.creationDate = creationDate;}

    public String getCreator() {return (creator == null ? "" : creator);}
    public void setCreator(String creator) {this.creator = creator;}

    public String getSubTypeName() {return (subTypeName == null ? "" : subTypeName);}
    public void setSubTypeName(String subTypeName) {this.subTypeName = subTypeName;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("idGroup: %s ", idGroup));
        sb.append(String.format("docId: %s ", docId));
        sb.append(String.format("folder: %s ", folder));
        sb.append(String.format("tab: %s ", tab));
        sb.append(String.format("f3: %s ", f3));
        sb.append(String.format("f4: %s ", f4));
        sb.append(String.format("f5: %s ", f5));
        sb.append(String.format("docTypeId: %s ", docTypeId));
        sb.append(String.format("docTypeName: %s ", docTypeName));
        sb.append(String.format("creationDate: %s ", creationDate));
        sb.append(String.format("creator: %s ", creator));
        sb.append(String.format("subTypeName: %s ", subTypeName));
        return sb.toString();
    }
}
