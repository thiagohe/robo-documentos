package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class VersionStatus {

    private Boolean isCheckedOut;
    private Boolean isVersioned;
    private CheckedOutUserInfo checkedOutUserInfo;

    public Boolean getIsCheckedOut() {return (isCheckedOut == null ? Boolean.FALSE : isCheckedOut);}
    public void setIsCheckedOut(Boolean isCheckedOut) {this.isCheckedOut = isCheckedOut;}

    public Boolean getIsVersioned() {return (isVersioned == null ? Boolean.FALSE : isVersioned);}
    public void setIsVersioned(Boolean isVersioned) {this.isVersioned = isVersioned;}

    public CheckedOutUserInfo getCheckedOutUserInfo() {return checkedOutUserInfo;}
    public void setCheckedOutUserInfo(CheckedOutUserInfo checkedOutUserInfo) {this.checkedOutUserInfo = checkedOutUserInfo;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("isCheckedOut: %s ", isCheckedOut));
        sb.append(String.format("isVersioned: %s ", isVersioned));
        return sb.toString();
    }
}
