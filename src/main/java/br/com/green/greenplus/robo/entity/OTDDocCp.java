package br.com.green.greenplus.robo.entity;

/**
 *
 * @author renato.paiva
 */
public class OTDDocCp {

    private int docId;
    private int cpId;
    private String value;

    public int getDocId() {return docId;}
    public void setDocId(int docId) {this.docId = docId;}

    public int getCpId() {return cpId;}
    public void setCpId(int cpId) {this.cpId = cpId;}

    public String getValue() {return (value == null ? "" : value);}
    public void setValue(String value) {this.value = value;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("docId: %s ", docId));
        sb.append(String.format("cpId: %s ", cpId));
        sb.append(String.format("value: %s ", value));
        return sb.toString();
    }
}
