package br.com.green.greenplus.robo.utils;

import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.com.green.greenplus.robo.main.Settings;

public class EmailUtils {
	private static final String REMETENTE = Settings.getProperty("email.remetente");
	private static final String SENHA = Settings.getProperty("email.senha");
	private static final String HOST_SMTP = Settings.getProperty("email.host");
	private static final String PORTA_SMTP = Settings.getProperty("email.porta");
	
	public static void enviarEmail(String assunto, String corpo, String destinatarios) throws Exception {
		Properties prop = new Properties();
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.host", HOST_SMTP);
		prop.put("mail.smtp.port", PORTA_SMTP);
		prop.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getDefaultInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(REMETENTE, SENHA);
			}
		});
		try {
			
			String htmlBody = "<strong>This is an HTML Message</strong>";
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(REMETENTE));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatarios));
			message.setSubject(assunto);
			
			MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
			mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
			mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
			mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
			mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
			mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
			CommandMap.setDefaultCommandMap(mc);
			message.setText(htmlBody);
			message.setContent(corpo, "text/html");
			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
}
