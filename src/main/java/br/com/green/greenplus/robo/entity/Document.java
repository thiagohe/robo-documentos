package br.com.green.greenplus.robo.entity;

import java.util.List;

import br.com.green.greenplus.robo.entity.imagenow.Info;
import br.com.green.greenplus.robo.entity.imagenow.Page;
import br.com.green.greenplus.robo.entity.imagenow.Properties;
import br.com.green.greenplus.robo.entity.imagenow.VersionInfo;
import br.com.green.greenplus.robo.entity.imagenow.WorkflowItem;

public class Document {

    private Info info;
    private List<WorkflowItem> workflowItems;
    private VersionInfo versionInfo;
    private List<Page> pages;
    private List<Properties> properties;

    public Info getInfo() {return info;}
    public void setInfo(Info info) {this.info = info;}

    public List<WorkflowItem> getWorkflowItems() {return workflowItems;}
    public void setWorkflowItems(List<WorkflowItem> workflowItems) {this.workflowItems = workflowItems;}

    public VersionInfo getVersionInfo() {return versionInfo;}
    public void setVersionInfo(VersionInfo versionInfo) {this.versionInfo = versionInfo;}

    public List<Page> getPages() {return pages;}
    public void setPages(List<Page> pages) {this.pages = pages;}

    public List<Properties> getProperties() {return properties;}
    public void setProperties(List<Properties> properties) {this.properties = properties;}
}
