package br.com.green.greenplus.robo.tipos.load;

import br.com.green.greenplus.robo.entity.Document;
import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.main.Manager;
import br.com.green.greenplus.robo.entity.OTDAtendimento;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.entity.OTDDocCp;
import br.com.green.greenplus.robo.entity.OTDDocument;
import br.com.green.greenplus.robo.entity.OTDPeriodoConta;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 *
 * @author renato.paiva
 */
public class ProcessJobIPPMG implements Callable<Boolean> {

    private final LoadTO loadTO;
    private final List<OTDImagenow> documentos;

    public ProcessJobIPPMG(LoadTO loadTO, List<OTDImagenow> documentos) {
        this.loadTO = loadTO;
        this.documentos = documentos;
    }

    /**
     * Job que insere uma lista de documentos no Imagenow e preenche o banco de
     * dados da GREEN com as informações dos documentos criados.
     *
     * Em seguida, atualiza os documentos na MV para indicar que eles já foram
     * processados.
     *
     * @throws Exception
     */
    @Override
    public Boolean call() throws Exception {
        String filaDestino = null;
        for (OTDImagenow documento : documentos) {
        	boolean sucessoImagenow = false;
			Document documentoImagenow = null;
			try {
				// ***************************************************************************
				// Consulta o documento no Imagenow
				// ***************************************************************************
				documentoImagenow = loadTO.getImagenowUtils().listarDocumento(documento.getDocId(), loadTO.getSessionHash());
				sucessoImagenow = true;
            	
            	Map<String, String> dados = loadTO.getDatabaseUtils().listarDadosCliente(documento.getCreator());
                String idGrupo = dados.get("grupo");
                String idCliente = dados.get("id_cliente");
                String idCompartimento = dados.get("compartimento");
            	
                // ***************************************************************************
                // Remove o documento e suas propriedades do banco de dados
                // (caso existam)
                // ***************************************************************************
                loadTO.getDatabaseUtils().removerGpDocument(documento.getDocId());

                if (idGrupo == null || idCliente == null) {
                    throw new Exception("Cliente/grupo não encontrados no banco de dados da GREEN");
                }

                // ***************************************************************************
                // Caso o documento não possua atendimento, consulta pelo
                // paciente
                // ***************************************************************************
                OTDAtendimento otdAtendimento;
                if (documento.getF2() == null || documento.getF2().isEmpty() || documento.getF2().equals("SEM REGISTRO")) {
                    String cdPaciente = documento.getF1();
                    otdAtendimento = loadTO.getWebserviceUtils().obterAtendimento(null, cdPaciente, loadTO.getCliente());
                    documento.setF2("LEGADO");
                } else {
                    String cdAtendimento = documento.getF2();
                    otdAtendimento = loadTO.getWebserviceUtils().obterAtendimento(cdAtendimento, null, loadTO.getCliente());
                    documento.setF1(otdAtendimento.getCdPaciente());
                }

                // ***************************************************************************
                // Atualiza a conta
                // ***************************************************************************
                String nrConta = "SEM CONTA";
                if (documento.getF3().isEmpty()) {
                    documento.setF3(nrConta);
                } else if (loadTO.getCliente().getTemCampoConta().equals("S")
                    && !documento.getF3().equals("SEM CONTA")) {
                    nrConta = documento.getF3();
                }

                OTDDocument d = new OTDDocument();
                d.setIdGroup(idGrupo);
                d.setDocId(documento.getDocId());
                d.setFolder(String.format("%s", otdAtendimento.getCdPaciente()));
                d.setTab(documento.getF2());
                d.setF3(documento.getF3());
                d.setF4(documento.getF4());
                d.setF5(documento.getF5());
                d.setDocTypeId(documento.getDocTypeId());
                d.setDocTypeName(documento.getDocTypeName());
                d.setCreator(documento.getCreator());

                // ***************************************************************************
                // Muda a data de criação do documento para o formato
                // correto
                // ***************************************************************************
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date creationDate = sdf.parse(documento.getCreationDate());
                sdf.applyPattern("d-M-yyyy");
                d.setCreationDate(sdf.format(creationDate));

                // ***************************************************************************
                // Inicializa a variável que vai conter os inserts da
                // aplicação
                // ***************************************************************************
                StringBuilder sql = new StringBuilder();
                sql.append("DECLARE v_document_id gp_document.id%TYPE := seq_gp_document.NEXTVAL; ");
                sql.append("BEGIN ");

                // ***************************************************************************
                // Monta o insert em GP_DOCUMENT
                // ***************************************************************************
                sql.append(loadTO.getDatabaseUtils().inserirGpDocument(d));

                // ***************************************************************************
                // Consulta as custom properties que o cliente utiliza
                // ***************************************************************************
                Map<String, Integer> propriedades = loadTO.getDatabaseUtils().listarCustomProperties(idCompartimento, idCliente);

                if (campoValido(propriedades, "CP_DATA_ATENDIMENTO", otdAtendimento.getHrAtendimento())) {
                    Integer id = propriedades.get("CP_DATA_ATENDIMENTO");
                    String valor = otdAtendimento.getHrAtendimento().replace("-", "/");
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_DATA_NASCIMENTO", otdAtendimento.getDtNascimento())) {
                    Integer id = propriedades.get("CP_DATA_NASCIMENTO");
                    String valor = otdAtendimento.getDtNascimento().replace("-", "/");
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_IDENTIDADE", otdAtendimento.getNrIdentidade())) {
                    Integer id = propriedades.get("CP_IDENTIDADE");
                    String valor = otdAtendimento.getNrIdentidade();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_NOME_MAE", otdAtendimento.getNmMae())) {
                    Integer id = propriedades.get("CP_NOME_MAE");
                    String valor = otdAtendimento.getNmMae();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_NOME_PACIENTE", otdAtendimento.getNmPaciente())) {
                    Integer id = propriedades.get("CP_NOME_PACIENTE");
                    String valor = otdAtendimento.getNmPaciente();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_NOME_MEDICO", otdAtendimento.getNmPrestador())) {
                    Integer id = propriedades.get("CP_NOME_MEDICO");
                    String valor = otdAtendimento.getNmPrestador();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_TIPO_ATENDIMENTO", otdAtendimento.getTpAtendimento())) {
                    Integer id = propriedades.get("CP_TIPO_ATENDIMENTO");
                    String valor = otdAtendimento.getTpAtendimento();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_CID", otdAtendimento.getCdCid())) {
                    Integer id = propriedades.get("CP_CID");
                    String valor = otdAtendimento.getCdCid();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_COD_CONVENIO", otdAtendimento.getCdConvenio())
                    && !otdAtendimento.getNmConvenio().isEmpty()) {
                    Integer id = propriedades.get("CP_COD_CONVENIO");
                    String valor = String.format("%s - %s", otdAtendimento.getCdConvenio(),
                        otdAtendimento.getNmConvenio());
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_PLANO", String.format("%s", otdAtendimento.getCdConPlan()))
                    && otdAtendimento.getDsConPla() != null) {
                    Integer id = propriedades.get("CP_PLANO");
                    String valor = String.format("%s - %s", otdAtendimento.getCdConPlan(),
                        otdAtendimento.getDsConPla());
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_SUBPLANO", otdAtendimento.getCdSubPlano())) {
                    Integer id = propriedades.get("CP_SUBPLANO");
                    String valor = otdAtendimento.getCdSubPlano();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                OTDPeriodoConta periodo = new OTDPeriodoConta();
                if (!documento.getF2().equals("SEM REGISTRO") && !nrConta.equals("SEM CONTA")) {
                    periodo = loadTO.getWebserviceUtils().obterPeriodoConta(documento.getF2(), nrConta,
                        loadTO.getCliente());
                }
                if (campoValido(propriedades, "CP_DATA_INICIO_CONTA", periodo.getDtInicioConta())) {
                    Integer id = propriedades.get("CP_DATA_INICIO_CONTA");
                    String valor = periodo.getDtInicioConta().replace("-", "/");
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_DATA_FINAL_CONTA", periodo.getDtFinalConta())) {
                    Integer id = propriedades.get("CP_DATA_FINAL_CONTA");
                    String valor = periodo.getDtFinalConta().replace("-", "/");
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_CPF", otdAtendimento.getNrCpf())) {
                    Integer id = propriedades.get("CP_CPF");
                    String valor = otdAtendimento.getNrCpf();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_CD_PACIENTE",
                    String.format("%s", otdAtendimento.getCdPaciente()))) {
                    Integer id = propriedades.get("CP_CD_PACIENTE");
                    String valor = documento.getF1();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_CD_ATENDIMENTO", otdAtendimento.getCdAtendimento())) {
                    Integer id = propriedades.get("CP_CD_ATENDIMENTO");
                    String valor = otdAtendimento.getCdAtendimento();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_CD_CONTA", nrConta)) {
                    Integer id = propriedades.get("CP_CD_CONTA");
                    String valor = nrConta;
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_TIPO_DOCUMENTO", documento.getDocTypeName())) {
                    Integer id = propriedades.get("CP_TIPO_DOCUMENTO");
                    String valor = documento.getDocTypeName();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_NOME_CRIADOR", documento.getCreator())) {
                    Integer id = propriedades.get("CP_NOME_CRIADOR");
                    String valor = documento.getCreator();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                if (campoValido(propriedades, "CP_NR_CNS", otdAtendimento.getNrCns())) {
                    Integer id = propriedades.get("CP_NR_CNS");
                    String valor = otdAtendimento.getNrCns();
                    sql.append(montarInsertGpDocCp(id, valor));
                }
                sql.append(" END; ");
                loadTO.getDatabaseUtils().executar(sql);
                documento.setNotes("");
                filaDestino = loadTO.getCliente().getFilaSucesso();
                Manager.logger.info(String.format("Documento inserido com sucesso: [%s]", documento.getDocId()));
            } catch (Exception e) {
                filaDestino = loadTO.getCliente().getFilaErro();
                Manager.logger.error(e.getMessage(), e);
                documento.setNotes((e.getMessage()).replace("\n", " "));
                Manager.logger.info(String.format("Documento com erro: [%s]", documento.getDocId()));
            } finally {
            	if (sucessoImagenow) {
            		try {
						loadTO.getImagenowUtils().atualizarChavesImagenow(documento, documentoImagenow, loadTO.getSessionHash());
					} catch (Exception e) {
						Manager.logger.error(e.getMessage(), e);
					}
        		}
                loadTO.getImagenowUtils().rotearDocumentoImagenow(documento.getWorkflowItemId(), filaDestino, loadTO);
            }
        }

        return true;
    }

    /**
     * Verifica se uma custom property existe e é válida.
     *
     * @param chave (Nome da custom property)
     * @param valor (Valor da custom property)
     * @return Boolean (TRUE se o campo estiver peenchido, FALSE caso contrário)
     */
    private Boolean campoValido(Map<String, Integer> propriedades, String chave, Object valor) {
        if (propriedades.isEmpty() || valor == null) {
            return false;
        }

        if (valor instanceof String) {
            String aux = (String) valor;
            if (chave.equals("CP_CD_CONTA") && aux.equals("SEM CONTA")) {
                return false;
            }
        }

        return (propriedades.containsKey(chave));
    }

    /**
     * Cria um comando INSERT para uma custom property.
     *
     * @param id (Id da custom property)
     * @param valor (Valor da custom property)
     * @return String (comando INSERT da custom property)
     * @throws Exception
     */
    private String montarInsertGpDocCp(Integer id, String valor) throws Exception {
        OTDDocCp cp = new OTDDocCp();
        cp.setCpId(id);
        cp.setValue(valor);
        return (loadTO.getDatabaseUtils().inserirGpDocCp(cp));
    }
}