package br.com.green.greenplus.robo.entity;

/**
 *
 * @author renato.paiva
 */
public class OTDPeriodoConta {

	private String dtFinalConta;
	private String dtInicioConta;

	public String getDtFinalConta() {
		return dtFinalConta;
	}

	public void setDtFinalConta(String dtFinalConta) {
		this.dtFinalConta = dtFinalConta;
	}

	public String getDtInicioConta() {
		return dtInicioConta;
	}

	public void setDtInicioConta(String dtInicioConta) {
		this.dtInicioConta = dtInicioConta;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append(String.format("dtFinalConta: %s ", dtFinalConta));
		sb.append(String.format("dtInicioConta: %s ", dtInicioConta));
		return sb.toString();
	}
}
