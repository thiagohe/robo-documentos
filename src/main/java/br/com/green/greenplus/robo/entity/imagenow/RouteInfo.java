package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class RouteInfo {

    private String originWorkflowQueueId;
    private String originWorkflowQueueName;
    private String destinationWorkflowQueueId;
    private String routeType;
    private String reason;

    public String getOriginWorkflowQueueId() {return (originWorkflowQueueId == null ? "" : originWorkflowQueueId);}
    public void setOriginWorkflowQueueId(String originWorkflowQueueId) {this.originWorkflowQueueId = originWorkflowQueueId;}

    public String getOriginWorkflowQueueName() {return (originWorkflowQueueName == null ? "" : originWorkflowQueueName);}
    public void setOriginWorkflowQueueName(String originWorkflowQueueName) {this.originWorkflowQueueName = originWorkflowQueueName;}

    public String getDestinationWorkflowQueueId() {return (destinationWorkflowQueueId == null ? "" : destinationWorkflowQueueId);}
    public void setDestinationWorkflowQueueId(String destinationWorkflowQueueId) {this.destinationWorkflowQueueId = destinationWorkflowQueueId;}

    public String getRouteType() {return (routeType == null ? "" : routeType);}
    public void setRouteType(String routeType) {this.routeType = routeType;}

    public String getReason() {return (reason == null ? "" : reason);}
    public void setReason(String reason) {this.reason = reason;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("originWorkflowQueueId: %s ", originWorkflowQueueId));
        sb.append(String.format("originWorkflowQueueName: %s ", originWorkflowQueueName));
        sb.append(String.format("destinationWorkflowQueueId: %s ", destinationWorkflowQueueId));
        sb.append(String.format("routeType: %s ", routeType));
        sb.append(String.format("reason: %s ", reason));
        return sb.toString();
    }
}
