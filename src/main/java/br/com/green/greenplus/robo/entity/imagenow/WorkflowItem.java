package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class WorkflowItem {

    private String id;

    public String getId() {return (id == null ? "" : id);}
    public void setId(String id) {this.id = id;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("id: %s ", id));
        return sb.toString();
    }
}
