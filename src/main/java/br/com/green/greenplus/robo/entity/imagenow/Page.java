package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class Page {

    private String id;
    private String name;
    private String extension;
    private Integer pageNumber;

    public String getId() {return (id == null ? "" : id);}
    public void setId(String id) {this.id = id;}

    public String getName() {return (name == null ? "" : name);}
    public void setName(String name) {this.name = name;}

    public String getExtension() {return (extension == null ? "" : extension);}
    public void setExtension(String extension) {this.extension = extension;}

    public Integer getPageNumber() {return (pageNumber == null ? 0 : pageNumber);}
    public void setPageNumber(Integer pageNumber) {this.pageNumber = pageNumber;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("id: %s ", id));
        sb.append(String.format("name: %s ", name));
        sb.append(String.format("extension: %s ", extension));
        sb.append(String.format("pageNumber: %s ", pageNumber));
        return sb.toString();
    }
}
