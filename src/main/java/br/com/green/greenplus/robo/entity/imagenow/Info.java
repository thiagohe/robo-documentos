package br.com.green.greenplus.robo.entity.imagenow;

/**
 *
 * @author renato.paiva
 */
public class Info {

    private String id;
    private String name;
    private Keys keys;
    private String notes;
    private VersionStatus versionStatus;
    private String locationId;

    public String getId() {return (id == null ? "" : id);}
    public void getId(String id) {this.id = id;}

    public String getName() {return (name == null ? "" : name);}
    public void setName(String name) {this.name = name;}

    public Keys getKeys() {return keys;}
    public void setKeys(Keys keys) {this.keys = keys;}

    public String getNotes() {return (notes == null ? "" : notes);}
    public void setNotes(String notes) {this.notes = notes;}

    public VersionStatus getVersionStatus() {return versionStatus;}
    public void setVersionStatus(VersionStatus versionStatus) {this.versionStatus = versionStatus;}

    public String getLocationId() {return (locationId == null ? "" : locationId);}
    public void setLocationId(String locationId) {this.locationId = locationId;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(String.format("id: %s ", id));
        sb.append(String.format("name: %s ", name));
        sb.append(String.format("notes: %s ", notes));
        sb.append(String.format("locationId: %s ", locationId));
        return sb.toString();
    }
}
