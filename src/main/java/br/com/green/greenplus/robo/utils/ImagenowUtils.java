package br.com.green.greenplus.robo.utils;

import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.entity.Document;
import br.com.green.greenplus.robo.entity.imagenow.Page;
import br.com.green.greenplus.robo.entity.imagenow.PageResponse;
import br.com.green.greenplus.robo.entity.imagenow.Properties;
import br.com.green.greenplus.robo.entity.imagenow.RouteInfo;
import br.com.green.greenplus.robo.main.Manager;
import br.com.green.greenplus.robo.main.Settings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.util.IOUtils;

/**
 *
 * @author renato.paiva
 */
public class ImagenowUtils {

    private final int sucesso = 200;
    private final String ecmServer = Settings.getProperty("ecm.server");
    private final String ecmUser = Settings.getProperty("ecm.user");
    private final String ecmPass = Settings.getProperty("ecm.pass");
    private final static  JsonUtils JSON_UTILS = new JsonUtils();

    /**
     * Abre uma conexão com o Imagenow via integration server.
     *
     * @return String (Hash da sessão do Imagenow)
     * @throws Exception
     */
    private String login() throws Exception {
        String result = null;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(String.format("%sconnection", ecmServer));
            httpGet.addHeader("X-IntegrationServer-Username", ecmUser);
            httpGet.addHeader("X-IntegrationServer-Password", ecmPass);

            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (response.getStatusLine().getStatusCode() == sucesso) {
                    result = response.getFirstHeader("X-IntegrationServer-Session-Hash").getValue();
                } else {
                	throw new Exception(String.format("Erro ao efetuar login no IMAGENOW [HTTP code: %s]", response.getStatusLine().getStatusCode()));
                }
            } catch (Exception e) {
            	Manager.logger.info(e.getMessage(), e);
            }
        }
        return result;
    }

    private boolean isSessionValido(String sessionHash) throws Exception {
        if (sessionHash == null) {
            return false;
        }
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(String.format("%sconnection", ecmServer));
            httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (response.getStatusLine().getStatusCode() == sucesso) {
                    return true;
                }
            } catch (Exception e) {
            	Manager.logger.info(e.getMessage(), e);
            }
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        }
        return false;
    }

    public String obterSessionHash(String sessionHash) throws Exception {
        if (isSessionValido(sessionHash)) {
            return sessionHash;
        }
        return login();
    }

    /**
     * Consulta as informações de um documento no Imagenow.
     *
     * @param docId (Id do documento no Imagenow)
     * @param sessionHash (String de conexão)
     * @return Document (Objeto contendo as chaves do documento)
     * @throws Exception
     */
    public Document listarDocumento(String docId, String sessionHash) throws Exception {
        Document retorno = null;
        
        String mensagemErro = String.format("Erro ao consultar documento no IMAGENOW [DocId: %s]", docId);
        
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(String.format("%sv3/document/%s", ecmServer, docId));
            httpGet.addHeader("accept", "application/json");
            httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (response.getStatusLine().getStatusCode() == sucesso) {
                    String str = EntityUtils.toString(response.getEntity(), "UTF-8");
                    Gson gson = new GsonBuilder().create();
                    retorno = gson.fromJson(str, Document.class);
                } else {
                	mensagemErro = String.format("Erro ao consultar documento no IMAGENOW [DocId: %s - HTTP code: %s]", docId, response.getStatusLine().getStatusCode());
                	throw new Exception();
                }
            } 
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception(mensagemErro);
        }
        return retorno;
    }

    /**
     * Atualiza as chaves de um documento no Imagenow.
     *
     * @param docId (DocId do documento no Imagenow)
     * @param json (JSON que vai atualizar as chaves do documento)
     * @param sessionHash (String de conexão)
     * @throws Exception
     */
    public void atualizarDocumento(String docId, String json, String sessionHash) throws Exception {
    	
    	String mensagemErro = String.format("Erro ao atualizar documento no IMAGENOW [DocId: %s]", docId);
    	
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPut httpPut = new HttpPut(String.format("%sv3/document/%s", ecmServer, docId));
            httpPut.addHeader("Content-Type", "application/json");
            httpPut.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

            StringEntity stringEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
            httpPut.setEntity(stringEntity);

            try (CloseableHttpResponse response = httpclient.execute(httpPut)) {
                if (response.getStatusLine().getStatusCode() != sucesso) {
                	mensagemErro = String.format("Erro ao atualizar documento no IMAGENOW [DocId: %s - HTTP code: %s]", docId, response.getStatusLine().getStatusCode());
                	throw new Exception();
                }
            } 
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception(mensagemErro);
        }
    }

    /**
     * Move um documento para uma fila de workflow no Imagenow.
     *
     * @param json (JSON que vai mover um documento para uma determinada fila)
     * @param workflowItemId (Id do item do workflow)
     * @param sessionHash (Hash da sessão do Imagenow)
     * @throws Exception
     */
    public void rotear(String json, String workflowItemId, String sessionHash) throws Exception {
    	String mensagemErro = "Erro ao rotear documento no IMAGENOW";
    	
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(
                String.format("%s/workflowItem/%s/routingAction", ecmServer, workflowItemId));
            httpPost.addHeader("Content-Type", "application/json");
            httpPost.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

            StringEntity se = new StringEntity(json, ContentType.APPLICATION_JSON);
            httpPost.setEntity(se);

            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                if (response.getStatusLine().getStatusCode() != sucesso) {
                	Manager.logger.info("Erro chamada URL = ->"+ String.format("%s/workflowItem/%s/routingAction", ecmServer, workflowItemId)+"<-");
                	Manager.logger.info("ERRO chamada json = ->"+json+"<-");
                	Manager.logger.info("CÓDIGO HTTP = "+response.getStatusLine().getStatusCode());
                	mensagemErro = String.format("Erro ao rotear documento no IMAGENOW [HTTP code: %s]", response.getStatusLine().getStatusCode());
                	throw new Exception();
                }
            } 
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception(mensagemErro);
        }
    }

    public void rotearDocumentoImagenow(String workflowItemId, String filaDestino, LoadTO loadTO)
        throws Exception {
        if (workflowItemId.isEmpty()) {
            return;
        }
        String idFilaOrigem = loadTO.getDatabaseUtils().listarFilaPorNome(loadTO.getCliente().getFilaProcesso());
        String idFilaDestino = loadTO.getDatabaseUtils().listarFilaPorNome(filaDestino);
        RouteInfo wf = new RouteInfo();
        wf.setDestinationWorkflowQueueId(idFilaDestino);
        wf.setOriginWorkflowQueueId(idFilaOrigem);
        wf.setOriginWorkflowQueueName(loadTO.getCliente().getFilaProcesso());
        wf.setReason("");
        wf.setRouteType("MANUAL");
        String json = loadTO.getJsonUtils().workflowToJson(wf);
        rotear(json, workflowItemId, loadTO.getSessionHash());
    }

    public List<Page> getPages(String docId, String sessionHash) throws Exception {
        List<Page> ret = null;
        
        String mensagemErro = "Erro ao obter páginas do documento no IMAGENOW";
        
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(String.format("%s/document/%s/page", Settings.getProperty("ecm.server"), docId));
            httpGet.addHeader("accept", "application/json");
            httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);
            
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (response.getStatusLine().getStatusCode() == 404) {
                	mensagemErro = String.format("Documento não encontrado [DocId: %s - HTTP code: %s]", docId, response.getStatusLine().getStatusCode());
                	throw new Exception();
                }
                if (response.getStatusLine().getStatusCode() != 200) {
                	mensagemErro = String.format("Erro ao obter páginas do documento no IMAGENOW [DocId: %s - HTTP code: %s]", docId, response.getStatusLine().getStatusCode());
                	throw new Exception();
                }
                String str = EntityUtils.toString(response.getEntity(), "UTF-8");
                Gson gson = new GsonBuilder().create();
                PageResponse dpr = gson.fromJson(str, PageResponse.class);
                ret = dpr.getPages();
            } 
        } catch (Exception e) {
        	Manager.logger.info(e.getMessage(), e);
        	throw new Exception(mensagemErro);
        }
        return ret;
    }

    public void delPageDoc(String docId, String pageId, String sessionHash) throws Exception {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpDelete httpDelete = new HttpDelete(String.format("%s/document/%s/page/%s", Settings.getProperty("ecm.server"), docId, pageId));
            httpDelete.addHeader("X-IntegrationServer-Session-Hash", sessionHash);
            try (CloseableHttpResponse response = httpclient.execute(httpDelete)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    Manager.logger.error("Erro ao remover página. DocID = " + docId + ". PageID = " + pageId);
                }
            }
        }
    }

    public void addPageDoc(String docId, String sessionHash, byte[] pdf) throws Exception {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(String.format("%s/document/%s/page", Settings.getProperty("ecm.server"), docId));
            httpPost.addHeader("Content-Type", "application/octet-stream");
            httpPost.addHeader("X-IntegrationServer-Session-Hash", sessionHash);
            httpPost.addHeader("X-IntegrationServer-Resource-Name", "PDF_" + docId);
            httpPost.addHeader("X-IntegrationServer-File-Size", "" + pdf.length);
            ByteArrayEntity bae = new ByteArrayEntity(pdf);
            httpPost.setEntity(bae);
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                if (response.getStatusLine().getStatusCode() != 201) {
                    Manager.logger.error("Erro ao adicionar PDF. DocID = " + docId);
                }
            }
        }
    }

    public byte[] getPageBytes(String docId, String pageId, String sessionHash) throws Exception {
        byte[] pagina = null;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(String.format("%s/document/%s/page/%s/file", Settings.getProperty("ecm.server"), docId, pageId));
            httpGet.addHeader("accept", "application/octet-stream");
            httpGet.addHeader("X-IntegrationServer-Session-Hash", sessionHash);

            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (response.getStatusLine().getStatusCode() == 200) {
                    try (InputStream is = response.getEntity().getContent()) {
                        pagina = IOUtils.toByteArray(is);
                    }
                }
            }
        }

        return pagina;
    }
    
	public void atualizarChavesImagenow(OTDImagenow documento, Document documentoImagenow, String sessionHash) throws Exception {
		documentoImagenow.getInfo().getKeys().setField1(documento.getF1());
		documentoImagenow.getInfo().getKeys().setField2(documento.getF2());
		documentoImagenow.getInfo().getKeys().setField3(documento.getF3());
		documentoImagenow.getInfo().getKeys().setField4(documento.getF4());
		documentoImagenow.getInfo().getKeys().setField5(documento.getF5());
		documentoImagenow.getInfo().setNotes(documento.getNotes());
		String json = JSON_UTILS.documentToJson(documentoImagenow);
		atualizarDocumento(documento.getDocId(), json, sessionHash);
	}

	public Properties getCustomProperty(Document documentoImagenow, String nmPropriedade) {
		Properties retorno = null;
		List<Properties> propriedades = documentoImagenow.getProperties();
		for (Properties propriedade : propriedades) {
			if (propriedade.getName().equals(nmPropriedade)) {
				retorno = propriedade;
				break;
			}
		}
		return retorno;
	}
	
	/**
     * Atualiza as chaves de um documento no Imagenow via integration server.
     *
     * @param docId (DocId do documento no Imagenow)
     * @param json (JSON que vai atualizar as chaves do documento)
     * @param hashSessao (String de conexão)
     * @throws ImagenowAtualizarException
     */
    public void atualizar(String docId, String json, String hashSessao) throws Exception {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            // Monta a URL do request
            HttpPut httpPut = new HttpPut(String.format("%s/document/%s", ecmServer, docId));

            // Monta os headers do request
            httpPut.addHeader("Content-Type", "application/json");
            httpPut.addHeader("X-IntegrationServer-Session-Hash", hashSessao);

            // Monta o body do request
            StringEntity stringEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
            httpPut.setEntity(stringEntity);

            // Executa a URL
            try (CloseableHttpResponse response = httpclient.execute(httpPut)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    // Monta a mensagem de erro do imagenow
                    int cdErro = response.getStatusLine().getStatusCode();
                    String erro = response.getLastHeader("X-IntegrationServer-Error-Message").getValue();
                    String msg = "atualizar [Status code: %s]. Reason: %s";
                    throw new Exception(String.format(msg, cdErro, erro));
                }

            } 

        } 
    }
    
    /**
     * Insere uma página em um documento no Imagenow via integration server.
     *
     * @param docId (DocId do documento no Imagenow)
     * @param page (ByteArray contendo a página)
     * @param pageNumber (Número da página)
     * @param hashSessao (String de conexão)
     * @throws ImagenowInserirPaginaException
     */
    public void inserirPagina(String docId, byte[] page, int pageNumber, String hashSessao) throws Exception {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            // Monta a URL do request
            HttpPost httpPost = new HttpPost(String.format("%s/document/%s/page", ecmServer, docId));

            // Monta os headers do request
            httpPost.addHeader("Content-Type", "application/octet-stream");
            httpPost.addHeader("X-IntegrationServer-Session-Hash", hashSessao);
            httpPost.addHeader("X-IntegrationServer-Resource-Name", String.format("page%d", pageNumber));

            // Monta o body do request
            ByteArrayEntity bae = new ByteArrayEntity(page);
            httpPost.setEntity(bae);

            // Executa a URL
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                if (response.getStatusLine().getStatusCode() != 201) {
                    // Monta a mensagem de erro do imagenow
                    int cdErro = response.getStatusLine().getStatusCode();
                    String erro = response.getLastHeader("X-IntegrationServer-Error-Message").getValue();
                    String msg = "inserirPagina [Status code: %s]. Reason: %s";
                    throw new Exception(String.format(msg, cdErro, erro));
                }

            } 

        } 
    }
}
