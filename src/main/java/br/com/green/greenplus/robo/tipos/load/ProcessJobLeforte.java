package br.com.green.greenplus.robo.tipos.load;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.entity.OTDAtendimentoLeforte;
import br.com.green.greenplus.robo.entity.OTDDocCp;
import br.com.green.greenplus.robo.entity.OTDDocument;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.entity.OTDPeriodoConta;
import br.com.green.greenplus.robo.entity.Document;
import br.com.green.greenplus.robo.main.Manager;

public class ProcessJobLeforte implements Callable<Boolean> {

	private final LoadTO loadTO;
	private final List<OTDImagenow> documentos;

	public ProcessJobLeforte(LoadTO loadTO, List<OTDImagenow> documentos) {
		this.loadTO = loadTO;
		this.documentos = documentos;
	}

	@Override
	public Boolean call() throws Exception {
		String filaDestino = null;
		for (OTDImagenow documento : documentos) {
			boolean sucessoImagenow = false;
			Document documentoImagenow = null;
			try {
				// ***************************************************************************
				// Consulta o documento no Imagenow
				// ***************************************************************************
				documentoImagenow = loadTO.getImagenowUtils().listarDocumento(documento.getDocId(), loadTO.getSessionHash());
				sucessoImagenow = true;
				
				// ***************************************************************************
				// Remove o documento e suas propriedades do banco de dados
				// (caso existam)
				// ***************************************************************************
				loadTO.getDatabaseUtils().removerGpDocument(documento.getDocId());

				Map<String, String> dados = loadTO.getDatabaseUtils().listarDadosCliente(documento.getCreator());
				String idGrupo = dados.get("grupo");
				String idCliente = dados.get("id_cliente");
				String idCompartimento = dados.get("compartimento");

				// ***************************************************************************
				// Caso o documento não possua atendimento, consulta pelo
				// paciente
				// ***************************************************************************
				OTDAtendimentoLeforte otdAtendimento;
				
				String valorCPUnidade = null; 
				
				if (documento.getF2().isEmpty() || documento.getF2().equals("SEM REGISTRO")) {
					throw new Exception("Documento sem atendimento preenchido");
				} else {
					String atendimento = documento.getF2();
					otdAtendimento = loadTO.getWebserviceUtils().listarAtendimentoLeforte(atendimento, loadTO.getCliente());
					documento.setF1(otdAtendimento.getCdPaciente() + "");
					
					try {
						Long cdMultiEmpresa = Long.parseLong(otdAtendimento.getCdMultiEmpresa());
						if (cdMultiEmpresa==1) {
							valorCPUnidade = "LIBERDADE";
						}
						else if (cdMultiEmpresa==7) {
							valorCPUnidade = "MORUMBI";
						} else {
							throw new Exception("Atendimento não corresponde a Liberdade ou Morumbi");
						}
					} catch (Exception ee) {
						throw new Exception("Código da unidade inválido");
					}
				}

				// ***************************************************************************
				// Atualiza a conta
				// ***************************************************************************
				String nrConta = "SEM CONTA";
				
				if (documento.getF3().isEmpty()) {
					documento.setF3(nrConta);
				} else if (loadTO.getCliente().getTemCampoConta().equals("S") && !documento.getF3().equals("SEM CONTA")) {
					nrConta = documento.getF3();
				}

				OTDDocument d = new OTDDocument();
				d.setIdGroup(idGrupo);
				d.setDocId(documento.getDocId());
				d.setFolder(otdAtendimento.getCdPaciente() + "");
				d.setTab(documento.getF2());
				d.setF3(documento.getF3());
				d.setF4(documento.getF4());
				d.setF5(documento.getF5());
				d.setDocTypeId(documento.getDocTypeId());
				d.setDocTypeName(documento.getDocTypeName());
				d.setCreator(documento.getCreator());
				

				// ***************************************************************************
				// Muda a data de criação do documento para o formato
				// correto
				// ***************************************************************************
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date creationDate = sdf.parse(documento.getCreationDate());
				sdf.applyPattern("d-M-yyyy");
				d.setCreationDate(sdf.format(creationDate));

				// ***************************************************************************
				// Inicializa a variável que vai conter os inserts da
				// aplicação
				// ***************************************************************************
				StringBuilder sql = new StringBuilder();
				sql.append("DECLARE v_document_id gp_document.id%TYPE := seq_gp_document.NEXTVAL; ");
				sql.append("BEGIN ");

				// ***************************************************************************
				// Monta o insert em GP_DOCUMENT
				// ***************************************************************************
				sql.append(loadTO.getDatabaseUtils().inserirGpDocument(d));

				// ***************************************************************************
				// Consulta as custom properties que o cliente utiliza
				// ***************************************************************************
				Map<String, Integer> propriedades = loadTO.getDatabaseUtils().listarCustomProperties(idCompartimento, idCliente);
				if (campoValido(propriedades, "CP_DATA_ATENDIMENTO", otdAtendimento.getDtAtendimento())) {
					Integer id = propriedades.get("CP_DATA_ATENDIMENTO");
					String valor = otdAtendimento.getDtAtendimento().replace("-", "/");
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if(campoValido(propriedades, "CP_UNIDADE", valorCPUnidade) ) {
					Integer id = propriedades.get("CP_UNIDADE");
					sql.append(montarInsertGpDocCp(id, valorCPUnidade));
					
					sql.append(" UPDATE GP_DOCUMENT SET SUB_TYPE_NAME = '"+valorCPUnidade+"' WHERE ID = v_document_id; ");
				}
				if(campoValido(propriedades, "CP_REGIONAL", otdAtendimento.getTpAtendimento()) ) { // setor
					Integer id = propriedades.get("CP_REGIONAL");
					String valor = null;
					
					if (otdAtendimento.getTpAtendimento().equals("I")) {
						valor = "INTERNACAO";
					}
					else if (otdAtendimento.getTpAtendimento().equals("E")) {
						valor = "SADT";
					}
					else if (otdAtendimento.getTpAtendimento().equals("U")) {
						valor = "PRONTO SOCORRO";
					}
					else if (otdAtendimento.getTpAtendimento().equals("A")) {
						valor = "AMBULATORIO";
					}
					
					if (valor!=null) {
						sql.append(montarInsertGpDocCp(id, valor));
						
						sql.append(" UPDATE GP_DOCUMENT SET SETOR = '"+valor+"' WHERE ID = v_document_id; ");
					}
					
				}
				if (campoValido(propriedades, "CP_DATA_NASCIMENTO", otdAtendimento.getDtNascimento())) {
					Integer id = propriedades.get("CP_DATA_NASCIMENTO");
					String valor = otdAtendimento.getDtNascimento().replace("-", "/");
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_IDENTIDADE", otdAtendimento.getNrIdentidade())) {
					Integer id = propriedades.get("CP_IDENTIDADE");
					String valor = otdAtendimento.getNrIdentidade();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_NOME_MAE", otdAtendimento.getNmMae())) {
					Integer id = propriedades.get("CP_NOME_MAE");
					String valor = otdAtendimento.getNmMae();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_NOME_PACIENTE", otdAtendimento.getNmPaciente())) {
					Integer id = propriedades.get("CP_NOME_PACIENTE");
					String valor = otdAtendimento.getNmPaciente();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_NOME_MEDICO", otdAtendimento.getNmPrestador())) {
					Integer id = propriedades.get("CP_NOME_MEDICO");
					String valor = otdAtendimento.getNmPrestador();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_TIPO_ATENDIMENTO", otdAtendimento.getTpAtendimento())) {
					Integer id = propriedades.get("CP_TIPO_ATENDIMENTO");
					String valor = otdAtendimento.getTpAtendimento();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_CID", otdAtendimento.getCdCid())) {
					Integer id = propriedades.get("CP_CID");
					String valor = otdAtendimento.getCdCid();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_COD_CONVENIO", otdAtendimento.getCdConvenio())
						&& !otdAtendimento.getNmConvenio().isEmpty()) {
					Integer id = propriedades.get("CP_COD_CONVENIO");
					String valor = otdAtendimento.getCdConvenio() + " - " + otdAtendimento.getNmConvenio();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_PLANO", otdAtendimento.getCdPlano())
						&& !otdAtendimento.getDsPlano().isEmpty()) {
					Integer id = propriedades.get("CP_PLANO");
					String valor = otdAtendimento.getCdPlano() + " - " + otdAtendimento.getDsPlano();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_SUBPLANO", otdAtendimento.getCdSubPlano())) {
					Integer id = propriedades.get("CP_SUBPLANO");
					String valor = otdAtendimento.getCdSubPlano();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				OTDPeriodoConta periodo = new OTDPeriodoConta();
				if (!documento.getF2().equals("SEM REGISTRO") && !nrConta.equals("SEM CONTA")) {
					periodo = loadTO.getWebserviceUtils().listarPeriodoContaLeforte(documento.getF2(), nrConta,
							loadTO.getCliente());
				}
				if (campoValido(propriedades, "CP_DATA_INICIO_CONTA", periodo.getDtInicioConta())) {
					Integer id = propriedades.get("CP_DATA_INICIO_CONTA");
					String valor = periodo.getDtInicioConta().replace("-", "/");
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_DATA_FINAL_CONTA", periodo.getDtFinalConta())) {
					Integer id = propriedades.get("CP_DATA_FINAL_CONTA");
					String valor = periodo.getDtFinalConta().replace("-", "/");
					sql.append(montarInsertGpDocCp(id, valor));
				}
				// if (campoValido("CP_CPF", otdAtendimento.getCpf())) {
				// Integer id = propriedades.get("CP_CPF");
				// String valor = otdAtendimento.getCpf();
				// sql.append(montarInsertGpDocCp(id, valor));
				// }
				if (campoValido(propriedades, "CP_CD_PACIENTE", otdAtendimento.getCdPaciente())) {
					Integer id = propriedades.get("CP_CD_PACIENTE");
					String valor = documento.getF1();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_CD_ATENDIMENTO", documento.getF2())) {
					Integer id = propriedades.get("CP_CD_ATENDIMENTO");
					String valor = documento.getF2();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_CD_CONTA", nrConta)) {
					Integer id = propriedades.get("CP_CD_CONTA");
					String valor = nrConta;
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_TIPO_DOCUMENTO", documento.getDocTypeName())) {
					Integer id = propriedades.get("CP_TIPO_DOCUMENTO");
					String valor = documento.getDocTypeName();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				if (campoValido(propriedades, "CP_NOME_CRIADOR", documento.getCreator())) {
					Integer id = propriedades.get("CP_NOME_CRIADOR");
					String valor = documento.getCreator();
					sql.append(montarInsertGpDocCp(id, valor));
				}
				sql.append(" END; ");
				loadTO.getDatabaseUtils().executar(sql);
				documento.setNotes("");
				filaDestino = loadTO.getCliente().getFilaSucesso();
				Manager.logger.info(String.format("Documento inserido com sucesso: [%s]", documento.getDocId()));
			} catch (Exception e) {
				filaDestino = loadTO.getCliente().getFilaErro();
				Manager.logger.error(e.getMessage(), e);
				documento.setNotes((e.getMessage()).replace("\n", " "));
				Manager.logger.info(String.format("Documento com erro: [%s]", documento.getDocId()));
			} finally {
				if (sucessoImagenow) {
					try {
						loadTO.getImagenowUtils().atualizarChavesImagenow(documento, documentoImagenow, loadTO.getSessionHash());
					} catch (Exception e) {
						Manager.logger.error(e.getMessage(), e);
					}
				}
				loadTO.getImagenowUtils().rotearDocumentoImagenow(documento.getWorkflowItemId(), filaDestino, loadTO);
			}
		}

		return true;
	}

	private Boolean campoValido(Map<String, Integer> propriedades, String chave, Object valor) {
		if (propriedades.isEmpty() || valor == null) {
			return false;
		}

		if (valor instanceof String) {
			String aux = (String) valor;
			if (chave.equals("CP_CD_CONTA") && aux.equals("SEM CONTA")) {
				return false;
			}
		}

		return (propriedades.containsKey(chave));
	}

	private String montarInsertGpDocCp(Integer id, String valor) throws Exception {
		OTDDocCp cp = new OTDDocCp();
		cp.setCpId(id);
		cp.setValue(valor);
		return (loadTO.getDatabaseUtils().inserirGpDocCp(cp));
	}

}
