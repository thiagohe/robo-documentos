package br.com.green.greenplus.robo.tipos.load;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.concurrent.Callable;

import br.com.green.greenplus.robo.entity.Cliente;
import br.com.green.greenplus.robo.entity.INKeys;
import br.com.green.greenplus.robo.entity.LoadTO;
import br.com.green.greenplus.robo.entity.OTDImagenow;
import br.com.green.greenplus.robo.entity.imagenow.Page;
import br.com.green.greenplus.robo.main.Manager;
import br.com.green.greenplus.robo.utils.DocumentUtils;

public class ProcessJobMVContratos implements Callable<Boolean> {
	private LoadTO loadTO;
	private List<OTDImagenow> documentos;
	private Cliente cliente;
	
	public ProcessJobMVContratos(LoadTO loadTO, List<OTDImagenow> documentos, Cliente cliente) {
		this.loadTO = loadTO;
		this.documentos = documentos;
		this.cliente = cliente;
	}
	
	@Override
	public Boolean call() throws Exception {
		for (OTDImagenow indoc : documentos) {
			String filaDestino = null;
			
			try {
				List<Page> pages = loadTO.getImagenowUtils().getPages(indoc.getDocId(), loadTO.getSessionHash());
				Page page = pages.get(0);
				String tipoDoc = page.getExtension().toUpperCase();
				
				// Se for PDF, converte para PNG.
				if (tipoDoc.equals("PDF")) {
					byte[] PDF = loadTO.getImagenowUtils().getPageBytes(indoc.getDocId(), page.getId(), loadTO.getSessionHash());
					loadTO.getImagenowUtils().delPageDoc(indoc.getDocId(), page.getId(), loadTO.getSessionHash());
					DocumentUtils.pdfToPNGAddPages(new ByteArrayInputStream(PDF), indoc.getDocId(), loadTO.getSessionHash(), loadTO.getImagenowUtils());
				}
				
				// reindex
				reindexarCampos(indoc);
				filaDestino = cliente.getFilaSucesso();
				Manager.logger.info(String.format("["+cliente.getNome()+"] Documento em "+tipoDoc+" processado com sucesso: [%s]", indoc.getDocId()));
			} catch (Exception e) {
				Manager.logger.error("["+cliente.getNome()+"] "+e.getMessage(), e);
				Manager.logger.info(String.format("Documento com erro: [%s]", indoc.getDocId()));
				filaDestino = cliente.getFilaErro();
			} finally {
				loadTO.getImagenowUtils().rotearDocumentoImagenow(indoc.getWorkflowItemId(), filaDestino, loadTO);
			}
		}
		
		return true;
	}
	
	private void reindexarCampos(OTDImagenow doc) throws Exception {
		INKeys keys = new INKeys();
        keys.setName(doc.getDocId());
        keys.setDrawerId(doc.getDrawerId());
        keys.setField1(doc.getF1());
        keys.setField2(doc.getF2());
        keys.setField3(doc.getF3());
        keys.setField4(doc.getF4());
        keys.setField5(doc.getDocId());
        keys.setDocumentType(doc.getDocTypeName());
        keys.setDrawer(doc.getDrawer());
        
		String json = loadTO.getJsonUtils().criarJsonDocumento(keys, null);
		loadTO.getImagenowUtils().atualizar(doc.getDocId(), json, loadTO.getSessionHash());
	}
	
}