package br.com.green.greenplus.robo.entity;

import br.com.green.greenplus.robo.utils.DatabaseUtils;
import br.com.green.greenplus.robo.utils.ImagenowUtils;
import br.com.green.greenplus.robo.utils.JsonUtils;
import br.com.green.greenplus.robo.utils.WebserviceUtils;

public class LoadTO {

	private DatabaseUtils databaseUtils;
	private WebserviceUtils webserviceUtils;
	private ImagenowUtils imagenowUtils;
	private JsonUtils jsonUtils;
	private Cliente cliente;
	private String sessionHash;

	public DatabaseUtils getDatabaseUtils() {
		return databaseUtils;
	}

	public void setDatabaseUtils(DatabaseUtils databaseUtils) {
		this.databaseUtils = databaseUtils;
	}

	public WebserviceUtils getWebserviceUtils() {
		return webserviceUtils;
	}

	public void setWebserviceUtils(WebserviceUtils webserviceUtils) {
		this.webserviceUtils = webserviceUtils;
	}

	public ImagenowUtils getImagenowUtils() {
		return imagenowUtils;
	}

	public void setImagenowUtils(ImagenowUtils imagenowUtils) {
		this.imagenowUtils = imagenowUtils;
	}

	public JsonUtils getJsonUtils() {
		return jsonUtils;
	}

	public void setJsonUtils(JsonUtils jsonUtils) {
		this.jsonUtils = jsonUtils;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getSessionHash() {
		return sessionHash;
	}

	public void setSessionHash(String sessionHash) {
		this.sessionHash = sessionHash;
	}

}
